import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *Classe qui permet de cr�� un objet Cadre qui va contenir tout notre Panneau
 *Principale. La classe initialise aussi les diff�rentes fonctionnalit�es du 
 *CadreAnimation.
 *   
 *@author Alexandre Audette G�nier
 *@version H2013
 *
 */
public class CadreAnimation implements Runnable, WindowListener {

	
	
	
	/*
	 CONSTANTE
	*/
	//Titre de monFrame
	public static String TITRE_MONFRAME = "Cadre Animation";
	
	/*
	 ATTRIBUTS
	*/	
	//Frame de mon application
	private JFrame monFrame;
	
	//Objet du Panneau principale a afficher
	private PanneauPrincipal panneauPrincipaleAff;
	
	//Objet du MenuBar a afficher
	private MonMenuBar monMenuBarAff;
	
	//Objet de dimensionnement de l'ecran
	private Dimension ecran;
	
	//Attributs specifiques pour la taille de l'ecran
	private int hauteur;
	private int largeur;

	/*
	CONSTRUCTEURS
	*/	
	
	/************************************************************************
	*Constructeur par d�fault qui cr� un cadre avec un PanneauPrincipal et 
	*un MenueBar.
	 ************************************************************************/		
	public  CadreAnimation(){ }
	
	/*
	ACCESSEURS
	*/
	/************************************************************************
	*M�thode qui Retourne la vitesse d'activation d'une sc�ne.
	* 
	*@return int La vitesse d'activation des sc�nes 		
	************************************************************************/
		
	
	
	
	
	/*
	MUTATEURS
	*/
	/************************************************************************
	*M�thode qui permet d'ajuster la vitesse de d�filement
	*@param vitesseActivation Nouvelle vitesse d'activation.	
	************************************************************************/	
	
	/*
	M�THODES OBLIGATOIRE PAR IMPL�MENTATION
	*/
	/************************************************************************
	*M�thode qui permet de configurer le JFrame(monFrame) de mon application et
	*les �couteurs de celui-ci.
	************************************************************************/
	public void run() {
		
		//Cr�ation de monFrame
		monFrame = new JFrame();
		
		//ajout d'un listener lorsque le frmae ouvre
		monFrame.addWindowListener(this);
		
		//Fermer la fen�tre l'orsque l'utilisateur appuit sur le x
		monFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Initialise la dimension a la taille de l'ecran
		ecran = Toolkit.getDefaultToolkit().getScreenSize();
		
		//Specification des dimensions
		largeur = ecran.width * 3 / 4;
		hauteur = ecran.height * 3 / 4 + 40;
		
		//Initialise les dimensions de monFrame
		monFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		monFrame.setSize(new Dimension(largeur, hauteur));
		
		//Centralise le positionnement de monFrame
		monFrame.setLocationRelativeTo(null);
		
		//Initialise le titre de mon monFrame
		monFrame.setTitle(TITRE_MONFRAME);
		
		//Ajouter le Panneau principal
		monFrame.getContentPane().add(panneauPrincipaleAff =
									  new PanneauPrincipal());
		//Ajouter mon menu bar
		monFrame.setJMenuBar(monMenuBarAff = 
				             new MonMenuBar( panneauPrincipaleAff));
		
		monFrame.validate();
		
		//Mettre les modifications visibles
		monFrame.setVisible(true);
	}
	
	/*
	M�THODES
	*/
	
	/*
	M�THODES OBLIGATOIRES PAR IMPLEMENTATION
	*/
	/**************************************************************************
	*Methode par implementation qui est appeller lorsque la le panneau
	*principal s'ouvre.
	*@param arg0 Fenetre sur lequel l'ecouteur a ete activer.
	 *************************************************************************/
	public void windowOpened(WindowEvent arg0) {
		//affichage d'un message de salutation
		JOptionPane.showMessageDialog(null,"<html>Bonjour, <br>" +
				"bienvenue dans notre programme d'animation en code java. " +
				"<br><br><hr><br>" +
				"Equipe 7: " +
				"<ul>" +
				"<li>Alexandre A. Genier</li>" +
				"<li>Michael Ho-Dac</li>" +
				"<li>Steven Lao</li>" +
				"<li>Sebastien Thiry</li>" +
				"</ul>" +
				"</html>");
		
		//Actualiser l'information sur le nombre de scene et la scene active
		panneauPrincipaleAff.getPanneauSelectionSceneActive().
													actualiseNombScene();
		
	}
	//Methode obligatoire par implementation
	public void windowClosing(WindowEvent arg0) {}
	//Methode obligatoire par implementation
	public void windowDeactivated(WindowEvent arg0) {}
	//Methode obligatoire par implementation
	public void windowDeiconified(WindowEvent arg0) {}
	//Methode obligatoire par implementation
	public void windowIconified(WindowEvent arg0) {}
	//Methode obligatoire par implementation
	public void windowActivated(WindowEvent arg0) {}
	//Methode obligatoire par implementation
	public void windowClosed(WindowEvent arg0) {}
	
	
	
	/*
	CLASSES INTERNE	
	*/
	 

}
