import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

/**
 * Classe qui permet de cr�� un objet Panneau qui va contenir tout nos
 * diff�rents panneau. Les diff�rents Panneau sont en classe interne de cette de
 * la classe PanneauPrincipal.
 * 
 * @author Alexandre Audette G�nier
 * @version H2013
 * 
 */

public class PanneauPrincipal extends JPanel {

	/*
	 * CONSTANTE
	 */

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * ATTRIBUTS
	 */
	// Objet Panneau pour Dessin�
	private PanneauDessin pDessin;

	// coleur de boutton par default
	private Color couleurParDefault;

	// Objet Panneau pour cr�� des fromes
	private PanneauCreationForme pCreationForme;

	// Objet Panneau pour s�lectionner la sc�ne active
	private PanneauSelectionSceneActive pSelectionSceneActive;

	// Objet Panneau pour de gestion de sc�ne
	private PanneauGestionScene pGestionScene;

	// Objet Panneau pour tracer et d�placer
	private PanneauModeEtVitesse pModeEtVitesse;

	// Reference vers l'animation a dessiner
	private Animation refAnimation;

	// Dimension du panneau principal
	private Dimension dimensionPanneauPrincipal;

	// Mode Deplace selectionner
	private boolean modeDeplaceActiver;

	// Mode Trace selectionner
	private boolean modeTraceActiver;
	
	// Mode Efface selectionner
	private boolean modeEffaceActiver;

	// Bouton demi-droite selectionner
	private boolean demiDroiteSelectionner;

	// Bouton Ellipse selectionner
	private boolean ellipseSelectionner;

	// Bouton Rectangle selectionner
	private boolean rectangleSelectionner;

	// Bouton Triangle selectionner
	private boolean triangleSelectionner;
	
	// Couleur de forme selectionner
	private Color couleurForme;
	
	// Variables booleennes pour la demi-droite
	private boolean clique1;
	private boolean clique2;

	/*
	 * CONSTRUCTEURS
	 */

	/************************************************************************
	 * Constructeur par d�fault qui cr� une animation avec un sc�ne vide, active
	 * cette sc�ne et met la vitesse par d�fault.
	 ************************************************************************/
	public PanneauPrincipal() {

		// Initialise les composantes du PanneauPrincipal
		initComposante();
	}

	/*
	 * ACCESSEURS
	 */
	/************************************************************************
	 * M�thode qui Retourne la dimension du panneau principale.
	 * 
	 * @return Dimension La dimension tu panneau princippal.
	 ************************************************************************/
	public Dimension getDimensionPanneauPrincipal() {
		// retourne la dimension actuelle du panneau principale
		return dimensionPanneauPrincipal;
	}

	/************************************************************************
	 * M�thode qui Retourne la reference de l'animation a dessiner
	 * 
	 * @return Animation La referrence de la'animation a dessiner.
	 ************************************************************************/
	public Animation getRefAnimation() {

		// retourne la reference ded l'animation a dessiner
		return refAnimation;
	}

	/************************************************************************
	 * M�thode qui Retourne la reference du panneau dessin
	 * 
	 * @return PanneauDessin La referrence du panneau dessin.
	 ************************************************************************/
	public PanneauDessin getPanneauDessin() {
		// retouner la reference du panneau dessin
		return pDessin;
	}

	/************************************************************************
	 * M�thode qui Retourne la reference du panneau selection de la scene Active
	 * 
	 * @return PanneauSelectionSceneActive La referrence du panneau de selection
	 *         de la scene Active.
	 ************************************************************************/
	public PanneauSelectionSceneActive getPanneauSelectionSceneActive() {
		// retouner la reference du panneau slection de la scene active
		return pSelectionSceneActive;
	}
	
	/************************************************************************
	 * M�thode qui retourne la reference de la couleur selectionnee par
	 * l'utilisateur
	 * 
	 * @return couleurForme La couleur selectionnee pour les nouvelles formes
	 ************************************************************************/
	public Color getCouleurForme() {
		return couleurForme;
	}

	/*
	 * MUTATEURS
	 */
	/************************************************************************
	 * M�thode qui permet d'ajuster la dimension du panneau principale
	 * 
	 * @param nouvelleDimension
	 *            Nouvelle dimension du panneau principale.
	 ************************************************************************/
	public void setDimensionPanneauPricipal(Dimension nouvelleDimension) {
		// ajuste la dimension
		dimensionPanneauPrincipal = nouvelleDimension;
	}

	/************************************************************************
	 * M�thode qui permet de modifier la couleur de la forme geometrique
	 * 
	 * @param c
	 *            Parametre selectionne qui modifie la couleur 
	 *            des figures geometriques.
	 ************************************************************************/
	public void setCouleurForme(Color c) {
		// ajuste la refference de l'animation a dessiner
		this.couleurForme = c;
	}
	
	/************************************************************************
	 * M�thode qui permet d'ajuster la referrence de l'animation a dessiner
	 * 
	 * @param nouvelleRefAnimation
	 *            Nouvelle referrencce d'animationa dessiner
	 ************************************************************************/
	public void setRefAnimation(Animation nouvelleRefAnimation) {
		// ajuste la refference de l'animation a dessiner
		refAnimation = nouvelleRefAnimation;
	}
	
	public void setClique1(boolean decision){
		this.clique1 = decision;
	}
	
	public void setClique2(boolean decision){
		this.clique2 = decision;
	}

	/*
	 * M�THODES
	 */
	/**********************************************************************
	 * Methode qui contient tous les parametres par default appeller lors de
	 * l'initialisation du panneau Principal
	 * 
	 *******************************************************************/
	public void initComposante() {

		// Initialise la dimension du tableau principale a la taille de l'ecran
		dimensionPanneauPrincipal = Toolkit.getDefaultToolkit().getScreenSize();

		// initialise mode trace et deplace
		modeTraceActiver = modeDeplaceActiver = false;
		
		// creation du panneau dessin avec les grandeur de l'ecran divise par
		// 1.75
		pDessin = new PanneauDessin(
				(int) (dimensionPanneauPrincipal.getHeight() / 1.75),
				(int) (dimensionPanneauPrincipal.getWidth() / 1.75));

		// Ajout du panneau creation forme
		pCreationForme = new PanneauCreationForme(160, 140);

		// Ajout du panneau selection de la scene active
		pSelectionSceneActive = new PanneauSelectionSceneActive(70, 500);

		// Ajout du panneau gestion de scene
		pModeEtVitesse = new PanneauModeEtVitesse(160, 200);

		// Ajout du panneau pour tracer ou deplacer
		pGestionScene = new PanneauGestionScene(160, 140);

		// permet de delimiter les limites
		this.setOpaque(true);

		// mettre la reference de l'animation a dessiner a null
		refAnimation = null;
		
		// Creation d'un panneau outil en GridLayout
		JPanel barreOutils = new JPanel();
		barreOutils.setLayout(new GridLayout(3, 1));

		// ajout des boutons dans le panneau selection scene active
		barreOutils.add(pModeEtVitesse);
		barreOutils.add(pCreationForme);
		barreOutils.add(pGestionScene);

		// mise en place du layout
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		// premiere case(0,0) le panneau de dessin
		c.gridx = 0;
		c.gridy = 0;
		add(pDessin, c);

		// deuxieme case (1,0) le panneau d'outil
		c.gridx = 1;
		c.gridy = 0;
		add(barreOutils, c);

		// derniere case (0,1) le panneau de scene active
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 1;
		add(pSelectionSceneActive, c);

		// validation des panneaux
		validate();

	}

	/****************
	 * Procedure qui donne un titre a la bordure
	 * 
	 * @param nom
	 *            Titre en 'String' de la bordure
	 * @return Titre de type 'Border'
	 ***************/
	public Border Titre(String nom) {
		return BorderFactory.createTitledBorder(nom);
	}

	/*
	 * CLASSES INTERNE
	 */

	/************************************************************************
	 * Classe interne qui permet la gestion du tableau dessin.
	 ************************************************************************/
	public class PanneauDessin extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * ATTRIBUTS
		 */
		// Lie a ce que Pierre Belisle fornie dans la classe
		// private RenderingHints renderHints;

		// Point ou le clique ce produit
		private Point2D pClic;

		// Point en mouvement
		private Point2D pMove;

		// Point 2 de la demi-droite
		private Point2D p2Dd;
		
		// Point temporaire (methode: deux clics)
		//private Point2D pTemp;

		// forme recu
		InterfaceForme formeDessiner = null;

		// Position du dans le tableau qui compose une scene
		private int position;

		/*
		 * CONSTRUCTEUR PAR DEFAULT
		 */
		/**********************************************************************
		 * Constructeur qui cr� un panneau dessin et apelle toutes ses
		 * composanntes par defaults.
		 * 
		 * @param initialiseDimension
		 *            Dimension du panneau dans lequel il est.
		 *********************************************************************/
		public PanneauDessin(int hauteur, int largeur) {
			// initialise les composantes du panneau dessin
			initComposante();

			// fournie par pierre Belisle
			this.setPreferredSize(new Dimension(largeur, hauteur));
			new RenderingHints(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);

		}

		/*
		 * M�THODES
		 */
		/**********************************************************************
		 * Methode qui contient tous les parametres par default appeller lors de
		 * l'initialisation du panneau dessin
		 * 
		 *********************************************************************/
		public void initComposante() {

			// position initialiser
			position = Scene.AUCUNE_FORME;

			// Ajouter un ecouteur au panneau dessin
			addMouseListener(new EcouteurSourisDessin());

			// Ajouter un ecouteur au panneau dessin
			addMouseMotionListener(new EcouteurSourisDessinDep());

			// couleur du panneau dessin (blanc)
			setBackground(Color.white);

			// validation des chagement apporter au panneau
			validate();
		}

		/**********************************************************************
		 * Methode qui permet de dessiner les forme contenue dans la scene
		 * active appeller a chaque rafraichissement.
		 * 
		 * @param elementADessiner
		 *            Graphique de la forme dessiner a l'ecran.
		 *********************************************************************/

		public void paintComponent(Graphics elementADessiner) {

			// iterateur nombre de forme a dessiner
			int nbFormeADessiner = 1;

			// conversion de lelement en graphique 2D
			Graphics2D elementFinalADessiner = (Graphics2D) elementADessiner;
			
			elementFinalADessiner.
				setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);

			// afficher le parent
			super.paintComponent(elementFinalADessiner);

			// section donnee par Pierre, si on bouge la sourie, la position en
			// x et en y est capte
			if (pMove != null) {
				int x = (int) pMove.getX();
				int y = (int) pMove.getY();

				elementFinalADessiner.setColor(java.awt.Color.BLACK);
				elementFinalADessiner.drawString("(" + x + "," + y + ")"
						, x, y);

			}

			elementFinalADessiner.setColor(Color.BLACK);
			// si il y a un animation
			if (getRefAnimation() != null) {

				// dessiner tous les formes de la scene active
				while (nbFormeADessiner <= refAnimation.getSceneActive()
						.getNbFormes()) {

					if (refAnimation.getSceneActive()
							.getForme(nbFormeADessiner) != null) {
						// dessine la forme selectionnner
						refAnimation.getSceneActive()
								.getForme(nbFormeADessiner)
								.dessine(elementFinalADessiner);

					}

					// prochaine forme
					nbFormeADessiner++;

				}

			}

			// dipsoser de l'element graphique
			elementFinalADessiner.dispose();

			// valider les changements
			validate();

		}

		/*
		 * CLASSE INTERNE AU PANNEAU DESSIN
		 */
		/*********************************************************************
		 * Classe interne qui permet l'ecoute des action de la souris dans le
		 * panneau dessin.
		 **********************************************************************/
		public class EcouteurSourisDessinDep implements MouseMotionListener {

			/*****************************************************************
			 * Methode par implementation qui permet de gerer les action lorsque
			 * le curseur est retnue dans le panneau
			 * 
			 * @param arg0
			 *            Information de l'action
			 *****************************************************************/
			public void mouseDragged(MouseEvent arg0) {

				if (position != Scene.AUCUNE_FORME && !modeEffaceActiver) {

					// position de la souris pendant le clic
					pMove = new Point2D(arg0.getX(), arg0.getY());
					// changer la postion de pClic
					refAnimation.getSceneActive().getForme(position + 1)
							.deplace(pClic, pMove);

				}	
				
				// redessiner le panneau dessin
				pDessin.repaint(); 

			}

			/*****************************************************************
			 * Methode par implementation qui permet de gerer les action lorsque
			 * le curseur est en mouvement dans le panneau
			 * 
			 * @param arg0
			 *            Information de l'action
			 *****************************************************************/
			public void mouseMoved(MouseEvent arg0) {

				// la position de la souris en tout temps
				pMove = new Point2D(arg0.getX(), arg0.getY());

				repaint();

			}

		}

		/*********************************************************************
		 * Classe interne qui permet l'ecoute des action de la souris dans le
		 * panneau dessin.
		 **********************************************************************/
		public class EcouteurSourisDessin implements MouseListener {

			/*****************************************************************
			 * Methode par implementation qui permet de gerer les action lorsque
			 * le curseur est appuyer dans le panneau
			 * 
			 * @param arg0
			 *            Information de l'action
			 *****************************************************************/
			public void mousePressed(MouseEvent arg0) {
				// si il pas d'animatione
				if (refAnimation == null) {
					// creer une animation vide
					refAnimation = new Animation();
				}

				
				// Position ou l'action de la souris a eu lieu
				pClic = new Point2D(arg0.getX(), arg0.getY());

				// si en mode trace
				if (modeTraceActiver) {
					
					// si Ellipse activer
					if (ellipseSelectionner) {
						
						// Dessine une ellipse au point cliquer
						formeDessiner = UtilitaireSaisie.ellipseSaisie(pClic);
						
						// sinon si Rectangle activer
					} else if (rectangleSelectionner) {
						
						// dessine un rectangle au point cliquer
						formeDessiner = UtilitaireSaisie.rectangleSaisie(pClic);
						
						// sinon si Triangle activer
					} else if (triangleSelectionner) {
						
						// dessine un triangle au point cliquer
						formeDessiner = UtilitaireSaisie.triangleSaisie(pClic);
						
						// sinon si Demi-droite activer
					} else if (demiDroiteSelectionner) {
						
//						// Methode: deux cliques
//						// Si le point temporaire est vide
//						if(pTemp == null){
//							// Autoriser la boucle
//							setClique1(true);
//						}
//						
//						// Pour le premier clic
//						if(clique1){
//							
//							// Le premier clic est garde en memoire
//							pTemp = pClic;
//							
//							// On change de boucle
//							setClique1(false);
//							setClique2(true);
//						}
//						// Pour le deuxieme clic
//						else if(clique2){
//							
//							// Un deuxieme clic est enregistre
//							pClic = new Point2D(arg0.getX(), arg0.getY());
//							setClique1(true);
//							
//							// creation de la demidroite
//							formeDessiner = new DemiDroite(pTemp, pClic,
//									getCouleurForme());
//							
//							// On vide le premier clique
//							pTemp = null;
//							
//						}
	
					}

					// sinon si mode deplace
				} else if (modeDeplaceActiver) {
					
					// voir si le point est contenue par une forme de la scene
					position = refAnimation.getSceneActive().localiserForme(
							pClic);
					
					// sinon si mode efface
//				} else if(modeEffaceActiver) {
//
//					// voir si le point est contenue par une forme de la scene
//					position = refAnimation.getSceneActive().localiserForme(
//							pClic);
//
//					// effacer la forme contenue
//					refAnimation.getSceneActive().detruireForme(position + 1);
		
				}
				// ajouter animation a scene active
				refAnimation.getSceneActive().AjouterForme(formeDessiner);

				// redessiner le panneau dessin
				pDessin.repaint();

			}

			/*****************************************************************
			 * Methode par implementation qui permet de gerer les action lorsque
			 * le curseur est relacher dans le panneau
			 * 
			 * @param arg0
			 *            Information de l'action
			 *****************************************************************/
			public void mouseReleased(MouseEvent arg0) {

				// deselectionne la forme creer
				position = Scene.AUCUNE_FORME;

				// si en mode trace est demidroite est selectionner
				if (modeTraceActiver && demiDroiteSelectionner) {
					
					// position au relachement
					p2Dd = new Point2D(arg0.getX(), arg0.getY());
					
					// creation de la demidroite
					formeDessiner = new DemiDroite(pClic, p2Dd,
							getCouleurForme());
					
					// ajouter animation a scene active
					refAnimation.getSceneActive().AjouterForme(formeDessiner);
					pDessin.repaint();
					
				

				}

			}

			// Methode obligatoire par implementation
			public void mouseClicked(MouseEvent arg0) {}

			// Methode obligatoire par implementation
			public void mouseEntered(MouseEvent arg0) {}

			// Methode obligatoire par implementation
			public void mouseExited(MouseEvent arg0) {}

		}

	}

	/************************************************************************
	 * Classe interne qui permet la gestion du tableau creation forme.
	 ************************************************************************/
	public class PanneauCreationForme extends JPanel implements ActionListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * ATTRIBUTS
		 */

		// Bouton Demi-Droite
		private JButton boutonDd = new JButton("Demi-Droite");

		// Bouton Ellipse
		private JButton boutonEllipse = new JButton("Ellipse");

		// Bouton Rectangle
		private JButton boutonTriangle = new JButton("Triangle");

		// Bouton Triangle
		private JButton boutonRectangle = new JButton("Rectangle");

		/*
		 * CONSTRUCTEUR PAR DEFAULT
		 */
		/**********************************************************************
		 * Constructeur qui cr� un panneau Creation forme et apelle toutes ses
		 * composanntes par defaults.
		 * 
		 * @param initialiseDimension
		 *            Dimension du panneau dans lequel il est.
		 *********************************************************************/
		public PanneauCreationForme(int hauteur, int largeur) {
			// initialise les composantes du panneau creation forme
			initComposante(hauteur, largeur);
		}

		/*
		 * M�THODES
		 */
		/**********************************************************************
		 * Methode qui contient tous les parametres par default appeller lors de
		 * l'initialisation du panneau creation de forme
		 * 
		 *********************************************************************/
		public void initComposante(int hauteur, int largeur) {

			this.setPreferredSize(new Dimension(hauteur, largeur));

			// titre du panneau affich�
			setBorder(Titre("Formes"));

			// ajout du bouton demi-droite
			add(boutonDd);
			// ajout du bouton ellipse
			add(boutonEllipse);
			// ajout du bouton triangle
			add(boutonTriangle);
			// ajout du bouton rectangle
			add(boutonRectangle);
			
			// Ajustement de la taille des boutons
			boutonEllipse.setPreferredSize(boutonDd.getPreferredSize());
			boutonRectangle.setPreferredSize(boutonDd.getPreferredSize());
			boutonTriangle.setPreferredSize(boutonDd.getPreferredSize());

			// abonner le bouton Demi-Droite a un ecouteur
			boutonDd.addActionListener(this);

			// abonner le bouton Ellipse a un ecouteur
			boutonEllipse.addActionListener(this);

			// abonner le bouton Trianlge a un ecouteur
			boutonTriangle.addActionListener(this);

			// abonner le bouton Trianlge a un ecouteur
			boutonRectangle.addActionListener(this);
			
			// ajout des validation
			validate();

		}

		/**********************************************************************
		 * Methode qui permet de mettre la selection des bouton du panneau a
		 * false.
		 *********************************************************************/
		public void setAllFormeSelectionFalse() {
			demiDroiteSelectionner = false;
			ellipseSelectionner = false;
			rectangleSelectionner = false;
			triangleSelectionner = false;
		}
		
		/**********************************************************************
		 * Methode qui remet tous les boutons actifs
		 *********************************************************************/
		public void setAllBoutonEnableTrue() {
			boutonDd.setEnabled(true);
			boutonEllipse.setEnabled(true);
			boutonTriangle.setEnabled(true);
			boutonRectangle.setEnabled(true);
		}
		
		/**********************************************************************
		 * Methode qui remet tous les boutons inactifs
		 *********************************************************************/
		public void setAllBoutonEnableFalse() {
			boutonDd.setEnabled(false);
			boutonEllipse.setEnabled(false);
			boutonTriangle.setEnabled(false);
			boutonRectangle.setEnabled(false);
		}

		/**********************************************************************
		 * Methode par implementation qui permet de gerer les action qui arrive
		 * dans le panneau panneau creation de forme.
		 * 
		 * @param e
		 *            Objet manipuler lors d'une action sur le panneau
		 *********************************************************************/
		public void actionPerformed(ActionEvent e) {
			
			// On initialise les boutons
			setAllBoutonEnableTrue();

			// si la source de l'action est boutton demi-droite
			if (e.getSource() == boutonDd) {
				// mise de tous les bouton a false
				setAllFormeSelectionFalse();
				// mise du bouton Dd a true
				demiDroiteSelectionner = true;
				// le bouton est desactive (feedback)
				boutonDd.setEnabled(false);

			}
			// si la source de l'action est boutton ellipse
			if (e.getSource() == boutonEllipse) {
				// mise de tous les bouton a false
				setAllFormeSelectionFalse();
				// mise du bouton Ellipse a true
				ellipseSelectionner = true;
				// le bouton est desactive
				boutonEllipse.setEnabled(false);

			}
			// si la source de l'action est boutton triangle
			if (e.getSource() == boutonTriangle) {
				// mise de tous les bouton a false
				setAllFormeSelectionFalse();
				// mise du bouton Triangle a true
				triangleSelectionner = true;
				// le bouton est desactive
				boutonTriangle.setEnabled(false);

			}
			// si la source de l'action est boutton rectangle
			if (e.getSource() == boutonRectangle) {
				// mise de tous les bouton a false
				setAllFormeSelectionFalse();
				// mise du bouton Rectangle a true
				rectangleSelectionner = true;
				// le bouton est desactive
				boutonRectangle.setEnabled(false);

			}

		}

	}

	/************************************************************************
	 * Classe interne qui permet la gestion du tableau selection de la scene
	 * active.
	 ************************************************************************/
	public class PanneauSelectionSceneActive extends JPanel implements
			ActionListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * ATTRIBUTS
		 */
		// Creation du bouton premiere scene
		JButton prem = new JButton(" |< ");
		// Creation du bouton scene precedente
		JButton prec = new JButton(" < ");
		// Creation du bouton prochaine scene
		JButton proc = new JButton(" > ");
		// Creation du bouton derniere scene
		JButton dern = new JButton(" >| ");

		// Creation d'une case de texte pour le # de la scene
		JTextField nomb = new JTextField(5);

		/*
		 * CONSTRUCTEUR PAR DEFAULT
		 */
		/**********************************************************************
		 * Constructeur qui cr� un panneau selection de la scene active forme et
		 * apelle toutes ses composanntes par defaults.
		 * 
		 * @param initialiseDimension
		 *            Dimension du panneau dans lequel il est.
		 *********************************************************************/
		public PanneauSelectionSceneActive(int hauteur, int largeur) {
			// initialise les composantes du panneau
			initComposante(hauteur, largeur);
		}

		/*
		 * M�THODES
		 */
		/**********************************************************************
		 * Methode qui contient tous les parametres par default appeller lors de
		 * l'initialisation du panneau Selection de la Scene Active
		 * 
		 *********************************************************************/
		public void initComposante(int hauteur, int largeur) {

			setPreferredSize(new Dimension(largeur, hauteur));

			// ajout d'un titre au panneau
			setBorder(Titre("Scene Active"));
			
			// Ajustement du JTextField
			nomb.setPreferredSize(prem.getPreferredSize());
			nomb.setEditable(false);
	
			// Alignement central
			nomb.setHorizontalAlignment(JTextField.CENTER);
			nomb.setEditable(false);

			// ajoouter au panneau le bouton premiere scene
			add(prem);
			// ajouter au panneau le bouton scene precedente
			add(prec);
			// ajouter au panneau le nombre de scene dans lanimation
			add(nomb);
			// ajouter au panneau le bouton prochaine scene
			add(proc);
			// ajouter au panneau le bouton derniere scene
			add(dern);

			// ajouter un ecouteur au bouton scene precedente
			prec.addActionListener(this);
			// ajouter un ecouteur au bouton premiere scene
			prem.addActionListener(this);

			if (refAnimation != null) {
				nomb.setText("" + refAnimation.getSceneActive() + "/"
						+ refAnimation.getNbScene());
			}

			// ajouter un ecouteur au bouton prochaine scene
			proc.addActionListener(this);
			// ajouter un ecouteur au bouton derniere scene
			dern.addActionListener(this);

			validate();

		}

		/********************************************************************
		 * Methode qui actualise l'information sur le nombre de scene et scene
		 * active.Si aucune animation 0/0 est afficher
		 * 
		 *********************************************************************/
		public void actualiseNombScene() {
	
			// si il y a une animation
			if (refAnimation != null) {
				
				// On se refere par rapport au nombre de scene cree
				int SceneAjuste = refAnimation.getNumSceneActive() + 1;
				
				nomb.setText("" + SceneAjuste + "/"
						+ refAnimation.getNbScene());
				validate();
				
				// sinon afficher 0/0 (ou "vide")
			} else {
				
				nomb.setText("Vide");
				
			}

		}

		/**********************************************************************
		 * Methode par implementation qui permet de gerer les action qui arrive
		 * dans le panneau panneau selection de la scene active.
		 * 
		 * @param e
		 *            Objet manipuler lors d'une action sur le panneau
		 *********************************************************************/
		public void actionPerformed(ActionEvent arg0) {
			if (refAnimation != null) {
				// si l'action est sur le bouton premiere scene
				if (arg0.getSource() == prem) {

					// activer la premiere scene
					refAnimation.activerPremiereScene();

					// si l'action est sur le bouton scene precedente
				} else if (arg0.getSource() == prec) {
					// activer la scene precedente
					refAnimation.activerScenePrecedente();

					// si l'action est sur le bouton prochaine scene
				} else if (arg0.getSource() == proc) {
					// activer la prochaine scene
					refAnimation.activerSceneSuivante();

					// si l'action est sur le bouton derniere scene
				} else if (arg0.getSource() == dern) {
					// activer la derniere scene
					refAnimation.activerDerniereScene();

				}
				// actualiser l'information scene active et nombre ttl de scene
				actualiseNombScene();
				// redessine le panneau dessin
				pDessin.repaint();
			}

		}

	}

	/************************************************************************
	 * Classe interne qui permet la gestion du tableau tracer ou deplacer ainsi
	 * que la vitesse active.
	 ************************************************************************/
	public class PanneauModeEtVitesse extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * ATTRIBUTS
		 */

		// Creation du bouton Jouer
		JButton play = new JButton("Jouer");

		// Creation des boutons radios
		JRadioButton trace = new JRadioButton("Tracer");
		JRadioButton deplac = new JRadioButton("Deplacer");
		JRadioButton efface = new JRadioButton("Effacer");
		
		// Regroupement des boutons radios
		ButtonGroup bMode = new ButtonGroup();


		/*
		 * CONSTRUCTEUR PAR DEFAULT
		 */
		/**********************************************************************
		 * Constructeur qui cr� un panneau mode et vitesse et apelle toutes ses
		 * composanntes par defaults.
		 * 
		 * @param initialiseDimension
		 *            Dimension du panneau dans lequel il est.
		 *********************************************************************/
		public PanneauModeEtVitesse(int hauteur, int largeur) {
			// initialise les composantes par default
			initComposante(hauteur, largeur);
		}

		/*
		 * M�THODES
		 */
		/**********************************************************************
		 * Methode qui contient tous les parametres par default appeller lors de
		 * l'initialisation du panneau mode et vitesse
		 * 
		 *********************************************************************/
		public void initComposante(int hauteur, int largeur) {

			setPreferredSize(new Dimension(largeur, hauteur));
			
			// Titre de la bordure
			setBorder(Titre("Modes"));

			// Creation d'un comboBox
			String[] chiffres = { "Defaut (300)", "100", "200", "400", "500" };

			// creation d'une boite de defilement pour la vitesse
			JComboBox<String> vitesse = new JComboBox
					<String>(chiffres);

			// empeche l'utilisateur d'ajouter de l'information
			vitesse.setEditable(false);

			// ajoute un ecouteur a la boite de defilement vitesse
			vitesse.addActionListener(new EcouteurVitesse(vitesse));

			// Creation d'un titre du comboBox
			JLabel titreVit = new JLabel("Vitesse: ");

			// Saut de ligne (improvisation)
			JLabel saut = new JLabel("<html><br><br></html>");
			
			// creation d'un separateur
			JSeparator sep = new JSeparator(SwingConstants.HORIZONTAL);
			sep.setPreferredSize(new Dimension(150, 1));
			
			// Ajouter les boutons radios dans le groupe bMode
			bMode.add(trace);
			bMode.add(deplac);
			//bMode.add(efface);
			
			// ajustement de taille
			play.setPreferredSize(vitesse.getPreferredSize());
			
			// ajouter bouton mode au panneau modeEtVitesse
			add(trace);
			add(deplac);
			//add(efface);

			// ajouter le separateur
			add(sep);
			
			// ajouer un titre de vitesse
			add(titreVit, BorderLayout.CENTER);
			
			// ajouter la boite de defilement au panneau modeEtVitesse
			add(vitesse);
			
			// ajouter un saut de ligne
			add(saut);
			
			// ajouter le bouton play au panneau modeEtVitesse
			add(play, BorderLayout.SOUTH);

			// abonner bouton play a l'ecouteur de souris
			play.addMouseListener(new EcouteurPlay(play));

			// abonner bouton play a un ecouteur par classe anonyme
			play.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent arg0) {

					if (refAnimation != null) {
						// Creation d�un Thread avec Runnable anonyme
						Thread t = new Thread(new Runnable() {

							// Cette m�thode sera ex�cut�e dans un processus
							// s�par�
							// du panneauPrincipal
							public void run() {
								for (int i = refAnimation.
										getNumSceneActive(); i < 
										refAnimation
										.getNbScene(); i++) {

									refAnimation.activerSceneSuivante();
									pSelectionSceneActive.actualiseNombScene();
									pDessin.repaint();

									// cette pause donne le temps � EDT
									// de faire le repaint
									try {
										Thread.sleep(refAnimation.
												getVitesse());
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
								}// fin du for
									// mise de la couleur du bouton play a
									// default
								pModeEtVitesse.play
										.setBackground(couleurParDefault);
							}// fin de run

						});// fin de Runnable

						// d�marre le processus
						t.start();

					}// fin de actionPerformed
				}
			});// fin de l��couteur
			
			// abonner bouton mode par classe anonyme
			trace.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
	
						// activer le mode trace
						modeTraceActiver = true;
						// desactiver le mode deplace
						modeDeplaceActiver = false;
						// desactiver le mode deplace
						modeEffaceActiver = false;

				}// fin de actionPerformed

			});// fin de l��couteur
			
			// abonner bouton play a un ecouteur par classe anonyme
			deplac.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
	
						// activer le mode trace
						modeTraceActiver = false;
						// desactiver le mode deplace
						modeDeplaceActiver = true;
						// desactiver le mode deplace
						modeEffaceActiver = false;

				}// fin de actionPerformed

			});// fin de l��couteur
			
			// abonner bouton mode par classe anonyme
			efface.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
	
						// desactiver le mode trace
						modeTraceActiver = false;
						// desactiver le mode deplace
						modeDeplaceActiver = false;
						// activer le mode deplace
						modeEffaceActiver = true;

				}// fin de actionPerformed

			});// fin de l��couteur

		}

	}

	/************************************************************************
	 * Classe interne qui permet la gestion du tableau gestion de la scene
	 * active.
	 ************************************************************************/
	public class PanneauGestionScene extends JPanel implements ActionListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/*
		 * ATTRIBUTS
		 */

		// creation du bouton ajouter scene
		JButton ajout = new JButton("Ajouter scene (+)");

		// creation du bouton supprimer scene
		JButton suppr = new JButton("Effacer scene (-)");

		// creation du bouton copier scene
		JButton copie = new JButton("Copier scene (=)");
		
		// creation du bouton couleur Forme
		JButton couleur = new JButton("Couleurs de forme");

		/*
		 * CONSTRUCTEUR PAR DEFAULT
		 */
		/**********************************************************************
		 * Constructeur qui cr� un panneau gestion de la scene active forme et
		 * apelle toutes ses composanntes par defaults.
		 * 
		 * @param initialiseDimension
		 *            Dimension du panneau dans lequel il est.
		 *********************************************************************/
		public PanneauGestionScene(int hauteur, int largeur) {
			// initialise les composantes du panneau
			initComposante(hauteur, largeur);
		}

		/*
		 * M�THODES
		 */
		/**********************************************************************
		 * Methode qui contient tous les parametres par default appeller lors de
		 * l'initialisation du panneau gestion de la scene
		 * 
		 *********************************************************************/
		public void initComposante(int hauteur, int largeur) {

			setPreferredSize(new Dimension(largeur, hauteur));
			
			setBorder(Titre("Options"));

			// ajouter le bouton ajouter au panneau gestion
			add(ajout);
			// ajouter le bouton supprimer au panneau gestion
			add(suppr);
			// ajouter le bouton copier au panneau gestion
			add(copie);
			// ajouter le bouton couleur au panneau gestion
			add(couleur);

			// ajouter un ecouteur au bouton ajouter
			ajout.addActionListener(this);
			// ajouter un ecouteur au bouton supprimer
			suppr.addActionListener(this);
			// ajouter un ecouteur au bouton copie
			copie.addActionListener(this);
			// ajouter un ecouteur au bouton couleur
			couleur.addActionListener(this);
			// validation des changements au panneau gestion
			validate();

		}

		/**********************************************************************
		 * Methode par implementation qui permet de gerer les action qui arrive
		 * dans le panneau panneau gestion de la scene active.
		 * 
		 * @param e
		 *            Objet manipuler lors d'une action sur le panneau
		 *********************************************************************/
		public void actionPerformed(ActionEvent e) {
			// si il y a une animation
			if (refAnimation != null) {
				// si bouton ajouter est appuyer
				if (e.getSource() == ajout) {
					// ajouter une scene
					refAnimation.nouvelleScene();
					
				}
				// si le bouton supprimer est appuyer
				if (e.getSource() == suppr) {
					// supprimer la scene active
					refAnimation.detruireSceneActive();

				}
				// si le bouton copier est appuyer
				if (e.getSource() == copie) {
					// copier la scene active
					refAnimation.copierSceneActive();

				}
				//si le bouton couleur est appuyer
				if(e.getSource() == couleur){

					setCouleurForme(JColorChooser.showDialog(null,
							"Choisissez votre couleur", Color.gray));

					// La couleur selectionnee sera utilisee dans
					// UtilitaireSaisie
					UtilitaireSaisie us = new UtilitaireSaisie();
					us.setCouleur(getCouleurForme());

					// La couleur du caractere du bouton change
					couleur.setForeground(getCouleurForme());
					
				}
				// actualiser l'informatrion sur la scene active et nbScene
				pSelectionSceneActive.actualiseNombScene();
				// redessiner le panneau dessin
				pDessin.repaint();

			}

		}

	}

	/************************************************************************
	 * Classe interne qui permet l'ecoute de laction de la souris sur le bouton
	 * mode
	 ************************************************************************/
	public class EcouteurPlay implements MouseListener {

		// reference du bouton abonner a l'ecouteur
		JButton refBouton;

		/**********************************************************************
		 * Constructeur qui cr� l'ecouteur de souris et recoit une reference du
		 * bouton qui l'utilise.
		 * 
		 * @param JButton
		 *            Button qui utilise l'ecouteur.
		 *********************************************************************/
		public EcouteurPlay(JButton refBouton) {
			// initiase la reference au bouton utiliser
			this.refBouton = refBouton;

		}

		/**********************************************************************
		 * Methode par implementation qui permet de gerer les action lorsque le
		 * curseur appuit sur un bouton.
		 * 
		 * @param arg0
		 *            Sur quelle Objet le curseur etait au moment du press
		 *********************************************************************/
		public void mousePressed(MouseEvent arg0) {
			// si il y a une animation
			if (refAnimation != null) {

				// si le bouton jouer est appuyer
				if (refBouton.getText().equals("Jouer")) {
					// prise de la couleur du bouton
					couleurParDefault = refBouton.getBackground();
					// mise de la couleur du bouton a vert
					refBouton.setBackground(Color.GREEN);
				}

			}
		}

		// Methode obligatoire par implementation
		public void mouseClicked(MouseEvent arg0) {
		}

		// Methode obligatoire par implementation
		public void mouseExited(MouseEvent arg0) {
		}

		// Methode obligatoire par implementation
		public void mouseEntered(MouseEvent arg0) {
		}

		// Methode obligatoire par implementation
		public void mouseReleased(MouseEvent arg0) {
		}

	}

	/************************************************************************
	 * Classe interne qui permet l'ecoute de laction de la souris sur le bouton
	 * mode
	 ************************************************************************/
	public class EcouteurVitesse implements ActionListener {

		// reference du bouton abonner a l'ecouteur
		JComboBox<String> refBouton;

		/**********************************************************************
		 * Constructeur qui cr� l'ecouteur d'action et recoit une reference de
		 * la boite de defilement qui l'utilise.
		 * 
		 * @param refBouton
		 *            Boite de defilement recu.
		 *********************************************************************/
		public EcouteurVitesse(JComboBox<String> refBouton) {
			// initiliase la reference
			this.refBouton = refBouton;

		}

		/**********************************************************************
		 * Methode par implementation qui permet de gerer les action qui arrive
		 * sur un objet.
		 * 
		 * @param arg0
		 *            Sur quelle Objet l'action a eu lieu
		 *********************************************************************/
		public void actionPerformed(ActionEvent arg0) {
			// si il y a une animation
			if (refAnimation != null) {
				// si on selectionne une vitesse de 300
				if (refBouton.getSelectedItem().equals("Defaut (300)")) {
					// ajuster la vitesse d'animation a 300
					refAnimation.setVitesse(300);

					// si on selectionne une vitesse de 100
				} else if (refBouton.getSelectedItem().equals("100")) {
					// ajuster la vitesse d'animation a 100
					refAnimation.setVitesse(100);

					// si on selectionne une vitesse de 200
				} else if (refBouton.getSelectedItem().equals("200")) {
					// ajuster la vitesse a 200
					refAnimation.setVitesse(200);

					// si on selectionne la vitesse a 400
				} else if (refBouton.getSelectedItem().equals("400")) {
					// ajuster la vitesse a 400
					refAnimation.setVitesse(400);

					// si on ajuste la vitesse a 500
				} else if (refBouton.getSelectedItem().equals("500")) {
					// ajuster la vitesse a 500
					refAnimation.setVitesse(500);
				}
			}
		}

	}

}
