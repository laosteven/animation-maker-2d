import java.awt.Color;

import javax.swing.JOptionPane;

//Petite classe utilitaire qui permet de saisir les diff�rentes formes
//Strag�gie : Les valeurs sont saisies � l'aide de JOptionPane
//            Si l'utilisateur annule � n'importe quel niveau, la m�thode
//            appel�e retourne null
//
//Aucun effort n'a �t� fait pour �viter la r�p�tition de code.

public class UtilitaireSaisie {

	private static Color couleur;
	
	//Accesseur et mutateur de couleur
	public static Color getCouleur(){
		return couleur;
	}
	
	public void setCouleur(Color c){
		UtilitaireSaisie.couleur = c;
	}
	
	/**
	 * Retourne une ellipse 
	 * @param pos la postion du centre
	 * @return null ou l'ellipse voulu
	 */
	public static  Ellipse ellipseSaisie(Point2D pos){
		Ellipse e = null;

		String rayonX = JOptionPane.showInputDialog(null, "Entrez le rayon en x :");

		if(rayonX != null){
			String rayonY = JOptionPane.showInputDialog(null, "Entrez le rayon en y :");

			if(rayonY != null)
				e = new Ellipse(pos, Double.valueOf(rayonX),
						Double.valueOf(rayonY), getCouleur());
		}
		return e;

	}

	/**
	 * Retourne un rectangle 
	 * @param pos la postion du centre
	 * @return null ou le rectangle voulu
	 */
	public static  Rectangle rectangleSaisie(Point2D pos){

		Rectangle r = new Rectangle();

		String l = JOptionPane.showInputDialog(null, "Entrez la largeur :");

		if(l != null){
			String h = JOptionPane.showInputDialog(null, "Entrez la hauteur :");

			if(h != null)
				r = new Rectangle(pos, Double.valueOf(h),
						Double.valueOf(l), getCouleur());
		}

		return r;
	}

	/**
	 * Retourne un triangle 
	 * @param pos la postion du centre
	 * @return null ou le triangle voulu
	 */
	public static  Triangle triangleSaisie(Point2D pos){
		Triangle t = new Triangle();

		String largeur = JOptionPane.showInputDialog(null, "Entrez la largeur :");

		if(largeur != null){
			String hauteur = JOptionPane.showInputDialog(null, "Entrez la hauteur :");

			if(hauteur != null){

				double l = Double.valueOf(largeur)/2;
				double h = Double.valueOf(hauteur)/2;

				Point2D a = new Point2D(pos.getX()-l,
						pos.getY()+h);


				Point2D b = new Point2D(pos.getX()+l,
						pos.getY()+h);

				Point2D c = new Point2D(pos.getX(),
						pos.getY()-h);

				t = new Triangle(a,b,c, getCouleur());
			}
		}
		return t;
	}
}