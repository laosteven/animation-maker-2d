import java.io.Serializable;

/**
 * 
 * Repr�sente un point dans un espace � deux dimensions
 *            
 * @author Pierre B�lisle
 * @version octobre 2005
 * Modifi� : f�vrier 2009 (ajout des m�thodes de calcul +, * et /)
 */
public class Point2D implements Serializable{
	
	/**
	 * Num�ro de version par d�faut
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	 * Dans le cadre du cours inf111 : 
	 * 
	 * Sert � montrer :
	 *           -la m�canique d'appel des constructeurs lors du new
	 *           -la d�finition des observateurs et des mutateurs
	 *           -la d�finition de quelques comportements possibles
	 *            d'objets de cette classe
	 */            

          //correspond � la coordonn�e (x,y) d'un point
          //dans le plan cart�sien en 2 dimensions
           private double x;
           private double y;
           
          /**************************************************
           * 
           * LES CONSTRUCTEURS
           * 
           **************************************************/
          /**
           * Constructeur par d�faut 
           *    construit le point (0,0)
           */
          public Point2D(){};
          
          /**
           * Constructeur par copie d'attributs
           *    construit le point (x,y)
           */
          public Point2D(double x, double y){
              this.x = x;
              this.y = y;
          }
          
          /**
           * Constructeur par copie d'objet 
           *    construit une copie de point
           */
          public Point2D(Point2D point){
              
              //on peut faire point.x et point.y puisqu'on est dans la classe
              this.x = point.x;
              this.y = point.y;
          }
                   
          
          /**************************************************
           * 
           * LES OBSERVATEURS
           * 
           **************************************************/
          /**
           * Retourne l'abscisse du point
           * @return La valeur de la coordonn�e x  
           */
          public double getX(){
               return x;
           }
           
          /**
           * Retourne l'ordonn�e du point
           * @return La valeur de la coordonn�e y  
           */
           public double getY(){
               return y;
           }
           
          /**************************************************
           * 
           * LES MUTATEURS
           * 
           **************************************************/

           /**
            * Permet de modifier l'abscisse du point
            * @param x La nouvelle coordonn�e x
            */
           public void setX(double x){
               this.x = x;
           }
           
           /**
            * Permet de modifier l'ordonn�e du point
            * @param y La nouvelle coordonn�e y
            */
           public void setY(double y){
               this.y = y;
           }
           
           
          /**************************************************
           * 
           * LES COMPORTEMENTS
           * 
           **************************************************/
           /**
            * Calcul la distance entre le point actuel 
            * et un autre point
            * @param point Le point � consid�rer
            * @return La racine carr�e de la somme des diff�rences en x et 
            *         en y au carr�
            */
           public double distance (Point2D point){
               return Math.sqrt(Math.pow(point.x-x,2) + Math.pow(point.y-y,2));
           }
           
           
           
           /**
            * Calcul le point au centre entre le point actuel et un autre point
            * @param point Le point � consid�rer
            * @return Les diff�rences en x,y divis� par 2
            * 
            */
           public Point2D milieu (Point2D point){
               return new Point2D(Math.abs((x-point.x)/2),
            		              Math.abs((y-point.y)/2));
           }
           
           /**
            * Retourne une copie du point actuel
            * @return Un nouveau point identique au point actuel 
            */
           public Point2D copie(){
               return new Point2D(x,y);
           }
                                 
           /**
            * Ajoute le point re�u au point actuel
            * @param point Le point � additionner
            */
           public void ajoute(Point2D point){
        	   x = x + point.x;
        	   y = y + point.y; 
           }
           
           /**
            * Ajoute la valeur re�ue au point actuel
            * en x et en y
            * @param valeur La valeur � ajouter 
            */
           public void ajoute(double valeur){
        	   x = x + valeur;
        	   y = y + valeur; 
           }
           
           /**
            * Multiplie le point re�u au point actuel
            * @param point Le point � multiplier
            */
           public void multiplie(Point2D point){
        	   x = x * point.x;
        	   y = y * point.y; 
           }
           
           /**
            * Multiplie la valeur re�ue au point actuel
            * en x et en y
            * @param valeur La valeur � multiplier 
            */
           public void multiplie(double valeur){
        	   x = x * valeur;
        	   y = y * valeur; 
           }           
                      
           /**
            * Divise le point re�u au point actuel
            * @param point Le point diviseur
            */
           public void divise(Point2D point){
        	   x = x / point.x;
        	   y = y / point.y; 
           }
           
           /**
            * Multiplie la valeur re�ue au point actuel
            * en x et en y
            * @param valeur La valeur � multiplier 
            */
           public void divise(double valeur){
        	   x = x * valeur;
        	   y = y * valeur; 
           }           
           
           /**
            * Retourne la somme entre deux points re�us
            * @param point1 Le premier point � additionner
            * @param point2 Le deuxi�me point � additionner
            * @return La somme entre point1 et point2
            */
           public static Point2D somme(Point2D point1, Point2D point2){
        	   return new Point2D(point1.x + point2.x,point1.y + point2.y); 
           }
           
           /**
            * Retourne la somme entre un point re�u et un scalaire
            * @param point Le point � additionner
            * @param scalaire La valeur scalaire
            * @return La somme entre le point et le scalaire en x et en y
            */
           public static Point2D somme(Point2D point, double scalaire){
        	   return new Point2D(point.x + scalaire,point.y + scalaire); 
           }
                      
           /**
            * Retourne le produit entre deux points re�us
            * @param point1 Le premier point � multiplier
            * @param point2 Le deuxi�me point � multiplier
            * @return La produit entre point1 et point2
            */
           public static Point2D produit(Point2D point1, Point2D point2){
        	   return new Point2D(point1.x * point2.x,point1.y * point2.y); 
           }
           
           /**
            * Retourne la produit entre un point re�u et un scalaire
            * @param point Le point � additionner
            * @param scalaire La valeur scalaire
            * @return La produit entre le point et le scalaire en x et en y
            */
           public static Point2D produit(Point2D point, double scalaire){
        	   return new Point2D(point.x * scalaire,point.y * scalaire); 
           }           

           /**
            * Retourne la quotient entre un point re�u et un scalaire
            * @param point Le point � diviser
            * @param scalaire La valeur scalaire
            * @return La quotient entre le point et le scalaire en x et en y
            */
           public static Point2D quotient(Point2D point, double scalaire){
        	   return new Point2D(point.x / scalaire,point.y / scalaire); 
           }           
           
           /**
            * Retourne le quotient entre deux points re�us
            * @param point1 Le point � diviser
            * @param point2 Le diviseur
            * @return La quotient entre point1 et point2
            */
           public static Point2D quotient(Point2D point1, Point2D point2){
        	   return new Point2D(point1.x / point2.x,point1.y / point2.y); 
           }
 
           /**
            * Retourne le point actuel sous forme de la cha�ne "(x,y)"
            * @return Une cha�ne repr�sentant la coordonn�e (x,y) du point
            */
           public String toString(){
               return "(" + x + "," + y + ")";
           }
            
           /**
            * Retourne vrai si le point actuel est �gal � un autre point
            * @param point Le point � consid�rer
            * @return this.getX() == point.getX() && this.getY() == point.getY() 
            */
           public boolean equals(Point2D point){
               return UtilitaireReel.equals(point.x, x) && 
                      UtilitaireReel.equals(point.y, y);
           }
           
           /******************************************************
           *CALCULER POINT DESTINATION
           *******************************************************
           *Un peu de trigo pour retrouver l'emplacement d'un point p2
           *� partir d'un point p1, d'un angle et d'une distance.
           *
           *45 degr�
           *distance de 1
           *
           *                                   .(p2)
           *
           *                      .(p1)
           *
           *
           *On retrouve la largeur avec distance * cos(angle_en_radian)
           *On retrouve la hauteur avec distance * sin(angle_en_radian)
           *
           *@param angleEnRadian L'angle o� se trouve le point destination
           *@param distance La distance o� se trouve le point destination
           *@param pointDepart Le point source du calcul
           *
           */
           public static Point2D calculerPointDestination(double angleEnRadian,                                         
                                                          double distance,
                                                          Point2D pointDepart){

        	       Point2D pointRetourne;
        	   
                   //Obtient les coordonn�es du point de depart
                   double posX = pointDepart.getX();
                   double posY = pointDepart.getY();

                   //Un peu de trigo pour retrouver les longueurs des 
                   //c�t�s du triangle form� par l'angle et l'hypoth�nuse 
                   //fournie.

                   //On calcule la longueur du d�placement en x et en y
           	       double longueurX = distance * Math.cos(angleEnRadian);
           	       double longueurY = distance * Math.sin(angleEnRadian);

           	       //On calcule le nouveau point par rapport 
           	       //� la longueur de d�placement
                   pointRetourne = new Point2D(posX + longueurX, 
                		                       posY + longueurY);
                   
                   return pointRetourne;

           }


           
           
           
           
           
       
}