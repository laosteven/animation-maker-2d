import java.awt.Graphics2D;
import java.awt.Polygon;
/**
*Classe qui h�rite de la classe AbstractForme et qui impl�mente Interfarce 
*forme. Permet de cr�� une figure de la forme d'un triangle et de savoir les
*points qu'il contient.
* 
*
* 
*@author Alexandre Audette Genier
*@version 17 f�vrier 2013
*
*/

public class Triangle extends AbstractForme {
	
	/*
	CONSTANTE
	*/
	
	/*
	ATTRIBUTS
	*/
	
	//deuxieme point du triangle
	private Point2D pos2;
	
	//troisieme point du triangle
	private Point2D pos3;
	
	//deplacement du triangle en X en memoire
	private double depXMem;
	//deplacement du triangle en Y en memoire
	private double depYMem;
	/*
	CONSTRUCTEURS
	*/
	
	//constructeur par default
	public Triangle() {
		
	}
	/**********************************************************************
	*Constructeur par copie d'attribut et re�oit les attributs de son parent
	* 
	*@param p    Objet de type Point2D d�sir�(Point du coin 1) 
	*@param pos2 Objet de type Point2D d�sir�(Point du coin 2)
	*@param pos3 Objet de type Point2D d�sir�(Point du coin 1)
	*@param c    Objet de type java.awt.Color d�sir�
	*
	*********************************************************************/
	public Triangle( Point2D p, Point2D pos2, Point2D pos3, java.awt.Color c) {
		super(p,c);
		this.pos2 = pos2;
		this.pos3 = pos3;
		
	}
	/*
	MUTATEURS
	*/
	/**********************************************************************
	*M�thode qui permet de modifier le point du deuxi�me coin.
	*
	*@param newPosition La nouvelle position du deuxi�me coin
	*********************************************************************/	
	public void setPos2(Point2D newPosition) {
		pos2 = newPosition;
	}
	/**********************************************************************
	*M�thode qui permet de modifier le point du troisi�me coin.
	*
	*@param newPosition La nouvelle position du troisi�me coin
	*********************************************************************/
	public void setPos3(Point2D newPosition) {
		pos3 = newPosition;
	}
	

	/*
	ACCESSEURS
	*/
	/**********************************************************************
	*M�thode qui Retourne  le point du deuxi�me coin.
	*
	*@return Point2D Objet de type Point2D le deuxi�me coin
	*********************************************************************/
	public Point2D getPos2() {
		return pos2;
	}
	/**********************************************************************
	*M�thode qui Retourne  le point du troisieme coin.
	*
	*@return Point2D Objet de type Point2D le troisieme coin
	*********************************************************************/
	public Point2D getPos3() {
		return pos3;
	}
	/*
	M�THODES
	*/
	
	/**********************************************************************
	 *Fonction Local qui calcule la coordonnee z du point C par rapport au
	 *segment AB
	 * 
	 *R�f�rence :
	 *  http://blogs.developpeur.org/kookiz/archive/2009/01/30/
	 *  c-d-terminer-si-un-point-est-l-int-rieur-d-un-triangle.aspx
	 *  
	 *@param Ax1 Coordonn� X du coin 1 
	 *@param Ay1 Coordonn� Y du coin 1
	 *@param Bx2 Coordonn� X du coin 2
	 *@param By2 Coordonn� Y du coin 2
	 *@param Cx3 Coordonn� X du coin 3
	 *@param Cy3 Coordonn� Y du coin 3  
	 *
	 *@return int la valeur z de l'�valuation 
	*********************************************************************/	
	private int z(int Ax1, int Ay1, int Bx2,
				  int By2, int Cx3, int Cy3 ) {
		//x1 (y2 - y3) + x2 (y3 - y1) + x3 (y1 - y2)
		
		return Ax1 * (By2 - Cy3) + Bx2 * (Cy3 - Ay1) + Cx3 * (Ay1 - By2);
	}
	
	
	
	//METHODES OBLIGATOIRE PAR IMPLEMENTATION
	
	/**********************************************************************
	*M�thode qui M�thode qui permet de v�rifier  si un point est contenue 
	*dans le triangle
	* 
	*@param  p       Objet de type POINT2D test�
	*@return boolean Si le point est contenue ou non dans triangle
	*********************************************************************/
	public boolean contient(Point2D p) {
		
		//position du premier coin du triangle en x
		double pos1X = super.getPos().getX();
		
		//postion du deuxieme coin du triangle en y
		double pos1Y = super.getPos().getY();
		
		//valeur z de p par rapport au segement AB
		int ABp;
		
		//valeur z de p par rapport au segement BC
		int BCp;
		
		//valeur z de p par rarpport au segement AC
		int ACp;
		
		//calcul de la valeur z de p par rapport au segement AB
		ABp = z((int)pos1X, (int)pos1Y, (int)pos2.getX(),
				(int)pos2.getY(),(int) p.getX(),(int) p.getY());
		
		//calcul de la valeur z de p par rapport au segement AC
		ACp = z((int)pos1X, (int)pos1Y, (int)p.getX(),(int) p.getY(),
				(int)pos3.getX(), (int)pos3.getY());
		
		//calcul de la valeur z de p par rapport au segement BC
		BCp = z((int)p.getX(), (int)p.getY(),(int)pos2.getX(),
				(int)pos2.getY(),(int)pos3.getX(),(int)pos3.getY());
		
		// si tous les valeurs z sont du meme signe
		if((ABp < 0 && BCp < 0 && ACp < 0) ||
		   (ABp > 0 && BCp > 0 && ACp > 0)	) {
			//point contenue dans le triangle
			return true;
		}
		//si les valeurs z ne sont pas du meme signe
		else
			//point non contenue dans le triangle
			return false;
	}
	
	/**
	*M�thode qui permet de dessiner un triangle a laide d'un polygon
	*
	*@param Graphics2D Objet utiliser pour dessiner le polygon triangle
	*/
    public void dessine(Graphics2D g2) {
    	
    	//configuration de la couleur
    	g2.setColor(getCouleur());
    	
    	//tableau des points X pour polygon
    	int [] pointX ={(int)super.getPos().getX(),
    					(int)getPos2().getX(), 
    					(int)getPos3().getX()};
    	
    	//tableau des points Y pour polygon
    	int[] pointY = {(int)super.getPos().getY(), 
    					(int)getPos2().getY(), 
    					(int)getPos3().getY()};
    	
    	//dessine le polygon 
    	g2.fillPolygon(pointX, pointY, 3);
    	
    	
    }
	
    /**
	*M�thode qui permet de deplacer le triangle par rapport a un point de 
	*depart et un point darriver.
	*
	*Strategie:
	*		- On doit garder le dernier deplacement en X et Y en memoire
	*		 pour eviter leffet d'un deplacement exponentielle
	*
	*@param Point2D point de depart du deplacement
	*@param Point2D point d'arriver du deplacement
	*/
    public void deplace(Point2D pointDepart, Point2D pointArrivee) { 	
    	
    	
    	//deplacement en x
    	double depX = pointArrivee.getX() - pointDepart.getX();
    	//deplacement en y
    	double depY = pointArrivee.getY() - pointDepart.getY();
    	
    	//nouvelle position coin 1
    	setPos(new Point2D((getPos().getX() + depX - depXMem),
    					   (getPos().getY() + depY - depYMem)));
    	//nouvelle position coin 2
    	setPos2(new Point2D((pos2.getX() + depX - depXMem),
    						(pos2.getY() + depY - depYMem)));
    		
    	//nouvelle position coin 3
    	setPos3(new Point2D((pos3.getX() + depX - depXMem),
    						(pos3.getY() + depY - depYMem)));
    	
    	//garder le depalcement X en memoire
    	depXMem = depX;
    	//garder le deplacement Y en memoire
    	depYMem = depY;  	
    	
    
    	
    }

    /**
	*M�thode qui permet de retourner information de la forme Triangle
	*
	*@return String  Retourne un information de la forme Triangle
	*/
    public String toString(){
    	return ("Triangle" + "\n" +"Coin Un:"+"(" + super.getPos().getX() +
    			"," + super.getPos().getY() + ")" + "Coin deux:" + "(" +
    			pos2.getX() + "," + pos2.getY() + ")" + "Coin trois:" + "(" +
    			pos3.getX() + "," + pos3.getY() + ")" + "Couleur:" + 
    			super.getCouleur() + "\n");
    }

}
