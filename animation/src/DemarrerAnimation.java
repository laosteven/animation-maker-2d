/**
 * Programme principal d'un projet d'animation de formes g�om�triques.
 * 
 * Voir �nonc� qui accompagne ce code.
 * 
 * @author pbelisle
 * @version H2013
 *
 */
public class DemarrerAnimation {

	public static void main(String[] args) {
		
		Thread t = new Thread(new CadreAnimation());
		t.start();
		
		
	}
	
}