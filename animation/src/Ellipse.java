import java.awt.Graphics2D;
/**
*Classe qui h�rite de la classe AbstractForme et qui impl�mente Interfarce 
*forme. Permet de cr�� une figure de la forme d'une Ellipse et de savoir les
*points qu'elle contient.
* 
*
* 
*@author Alexandre Audette Genier
*@version 17 f�vrier 2013
*
*/

public class Ellipse extends AbstractForme {
	
	/*
	CONSTANTE
	*/
	
	/*
	ATTRIBUTS
	*/	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//rayon x
	private double rayonX;
	
	//rayon y
	private double rayonY;
	
	
	/*
	CONSTRUCTEURS
	*/
	
	// constructeur par default
	public Ellipse() {
		
	}
	
	/**********************************************************************
	*Constructeur par copie d'attribut et re�oit les attributs de son parent
	* 
	*@param p      Objet de type Point2D d�sir�(point Milieu)
	*@param c      Objet de type java.awt.Color d�sir�
	*@param rayonX Rayon X d�sir�e du rectangle
	*@param rayonY Rayon Y d�sir�e du rectangle
	*********************************************************************/	
	public Ellipse(Point2D p,double rayonX,double rayonY, java.awt.Color c) {
		super(p,c);
		this.rayonX = rayonX;
		this.rayonY = rayonY;
	}
	
	/*
	MUTATEURS
	*/
	/**********************************************************************
	*M�thode qui Modifier la valeur du rayon x de l'�llipse
	* 
	*@param rayonX  Le rayon X d�sir�e du rectangle
	*********************************************************************/	
	public void setRayonX(double rayonX) {
		this.rayonX = rayonX;
	}
	
	/**********************************************************************
	*M�thode qui Modifier la valeur du rayon y de l'�llipse
	* 
	*@param rayonY  Le rayon Y d�sir�e du rectangle
	*********************************************************************/
	public void setRayonY(double rayonY) {
		this.rayonY = rayonY;
	}
	
	/*
	ACCESSEURS
	*/
	/**********************************************************************
	*M�thode qui Retourne la valeur du rayon x de l'�llipse 
	* 
	* @return double Le rayon X de l'�llipse
	*********************************************************************/
	public double getRayonX() {
		return rayonX;
	}
	/**********************************************************************
	*M�thode qui Retourne la valeur du rayon y de l'�llipse 
	* 
	*@return double Le rayon Y de l'�llipse
	*********************************************************************/
	public double getRayonY() {
		return rayonY;
	}
	/*
	M�THODES
	*/
	
	
	//METHODES OBLIGATOIRE PAR IMPLEMENTATION
	
	/**********************************************************************
	*M�thode qui permet de v�rifier  si un point est contenue dans l'�llipse
	* 
	*@param  p       Objet de type Point2D test�
	*@return boolean Si le point est contenue ou non dans l'�llipse
	*********************************************************************/
	public boolean contient(Point2D p){
		//position du centrre de lellipse en x
		double positionCentreEllipseX = super.getPos().getX();
		//position du centre de lellipse en y
		double positionCentreEllipseY = super.getPos().getY();
			
		//si le point est contenue dans lellipse
		if( ((Math.pow((positionCentreEllipseX - p.getX()),2)) /
			( Math.pow(rayonX, 2)) + 				
			( Math.pow((positionCentreEllipseY - p.getY()),2))  /
			( Math.pow(rayonY, 2))  ) <=1) {
				
			return true;
				
		}
		//si le point nest pas contenue dasn lellipse
		else 
			return false;
				
		}
		
	/**
	*M�thode qui permet de dessiner un ellipse.
	*
	*@param Graphics2D Objet utiliser pour dessiner l'ellipse
	*/
	public void dessine(Graphics2D g2){
		
		//configuration de la couleur
		g2.setColor(getCouleur());
		
		//dessiner l'ellipse
		g2.fillArc((int)super.getPos().getX(),(int)super.getPos().getY(),
				   (int)rayonX, (int)rayonY, 0, 360);
	    	
	}
		
	/**
	*M�thode qui permet de depalcer un Ellipse.
	*
	*@param Point2D point de depart du deplacement
	*@param Point2D point d'arriver du deplacement
	*/
	public void deplace(Point2D pointDepart, Point2D pointArrivee){
		//changement de la position au point d'arriver
		super.setPos(pointArrivee);	    	
	}
	/**
	*M�thode qui permet de retourner information de la forme Ellipse
	*
	*@return String  Retourne un information de la forme Ellipse
	*/
	public String toString(){
	    return ("Ellipse" + "\n" +"Position:"+"(" + super.getPos().getX() +
	    		"," + super.getPos().getY() + ")" + "Rayon X:"+ rayonX +
	    		"Rayon Y:"+ rayonY + "Couleur:"+ super.getCouleur() + "\n");
	}

}
