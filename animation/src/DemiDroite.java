import java.awt.Graphics2D;
/**
*Classe qui h�rite de la classe AbstractForme et qui impl�mente Interfarce 
*forme. Permet de cr�� une figure Demi-droite et de savoir les points qu'elle
*contient.
* 
*
* 
*@author Alexandre Audette Genier
*@version 17 f�vrier 2013
*
*/

public class DemiDroite extends AbstractForme {
	
	/*
	CONSTANTE
	*/
	//variable repr�sentant une pente vertical
	private final double PENTE_VERTICAL = Double.NaN;
	
	/*
	ATTRIBUTS
	*/
	//positon du parent
	private Point2D p1 = super.getPos();
	
	//Deuxieme position formant la demi-droite
	private Point2D p2;
	
	//deplacement en X en memoire
	private double depXMem;
	//deplacement en Y en memoire
	private double depYMem;
	/*
	CONSTRUCTEURS
	*/
	
	//constructeur par default
	public DemiDroite(){
			
	}
	/**********************************************************************
	*Constructeur par copie d'attribut et re�oit les attributs de son parent
	* 
	*@param p  Objet de type Point2D d�sir�(Premier point)
	*@param c  Objet de type java.awt.Color d�sir�
	*@param p2 Le deuxi�me point de la demi-droite d�sir�
	*
	*********************************************************************/
	public DemiDroite(Point2D p, Point2D p2, java.awt.Color c){
		super(p, c);
		this.p2 = new Point2D(p2);		
	}	
	/*
	MUTATEURS
	*/
	/**********************************************************************
	*M�thode qui Change la position du deuxi�me de la demi-droite
	* 
	*@param p1 Objet de type Point2D 
	*********************************************************************/
	public void setPos1(Point2D p1) {
		
		this.p1 = p1;
	}
	/**********************************************************************
	*M�thode qui Change la position du deuxi�me de la demi-droite
	* 
	*@param p2 Objet de type Point2D 
	*********************************************************************/
	public void setPos2(Point2D p2) {
		
		this.p2 = p2;
	}
	
	/*
	ACCESSEURS
	*/
	/**********************************************************************
	*M�thode qui Retourne l'objet qui constitue le premier point de 
	*la demi-droite
	* 
	*@return Point2D L'objet du premier point
	*********************************************************************/
	public Point2D getP1() {
		return p1;
	}
	
	/**********************************************************************
	*M�thode qui Retourne l'objet qui constitue le deuxi�me point de 
	*la demi-droite
	* 
	*@return Point2D L'objet du deuxi�me point
	*********************************************************************/
	public Point2D getP2() {
		return p2;
	}
	
	/*
	M�THODES
	*/
	/**********************************************************************
	*M�thode qui calcule la pente de la demi-droite.
	* 
	*@return double La pente de la demi-droite
	*********************************************************************/	
	public double pente(){	
		
		//si ces une pente verticale
		if( UtilitaireReel.equals(p1.getX(), p2.getX())) {
			return PENTE_VERTICAL;
			
		}
		//sinon calcul de la pente
		else
		{
			return (p2.getY() - p1.getY()) / (p2.getX() - p1.getX());
		}
	}
	/**********************************************************************
	*M�thode qui calcule l'ordonn�e � l'origine de la demi-droite
	* 
	*@return double L'ordonn�e � l'origine
	*********************************************************************/		
	public double ordonneeOrigine() {
		
		//si la pente est vertical
		if ( UtilitaireReel.equals(pente(), PENTE_VERTICAL)) {
			return p1.getX();
		}
		//sinon calcul de lordonne a lorigine
		else 
		{
			return (p1.getY() - pente() * p1.getX());
		}
	}
	/**********************************************************************
	*M�thode qui V�rifier si un point est sur la demi-droite
	* 
	*@param  p       Point2D qui est test�.
	*@return boolean Si le point est sur la droite ou non.
	*********************************************************************/
	public boolean verifieEquationDeLaDroite(Point2D p) {
	
		//si le point est sur la droite
		if ( (UtilitaireReel.equals(pente(),  PENTE_VERTICAL) && 
			  UtilitaireReel.equals(p.getX(), p1.getX()) )||
			 (UtilitaireReel.equals(p.getY(), (pente() * p.getX() + ordonneeOrigine())))) {
			return true;
		}
		//si le point n'est pas sur la droite
		else{
			return false;
		}
	}	

	//METHODE OBLIGATOIRE PAR IMPLEMENTATION
	
	/**********************************************************************
	*M�thode qui V�rifier si un point contenue dans  la demi-droite
	* 
	*@param  p       Point2D qui est test�.
	*@return boolean Si le point est contenue la droite ou non.
	*********************************************************************/
	public boolean contient(Point2D p) {	
		
		//si point est contenu dans la demi-droite
		if (!(p.getX() < p1.getX() && p.getX() < p2.getX()) &&
			!(p.getX() > p1.getX() && p.getX() > p2.getX()) &&
			verifieEquationDeLaDroite(p)){
			return true;
		}
		//sinon point n'est pas contenue dans la demi-droite
		else
		{
			return false;
		}
	}
	
	/**
	*M�thode qui permet de dessiner une demi-droite.
	*
	*@param Graphics2D Objet utiliser pour dessiner la demi-droite
	*/
    public void dessine(Graphics2D g2) {
    	
    	//configuration de la couleur
    	g2.setColor(getCouleur());
    	
    	//dessine une ligne
    	g2.drawLine((int)p1.getX(),(int) p1.getY(),
    				(int)p2.getX(),(int) p2.getY());
    }
	
    /**
	*M�thode qui permet de deplacer un Ellipse selon son point de depart et 
	*darriver.
	*	
	*Strategie:
	*		- On doit garder le dernier deplacement en X et Y en memoire
	*		 pour eviter leffet d'un deplacement exponentielle
	*
	*@param Point2D point de depart du deplacement
	*@param Point2D point d'arriver du deplacement
	*/
    public void deplace(Point2D pointDepart, Point2D pointArrivee) { 	
    	
    	
    	//deplacement en x
    	double depX = pointArrivee.getX() - pointDepart.getX();
    	//deplacement en y
    	double depY = pointArrivee.getY() - pointDepart.getY();
    	
    	//nouvelle position point 1
    	setPos1(new Point2D((p1.getX() + depX - depXMem),
    						(p1.getY() + depY - depYMem)));
    	
    	//nouvelle position point 2
    	setPos2(new Point2D((p2.getX() + depX - depXMem),
    						(p2.getY() + depY - depYMem)));
    		
    	
    	
    	//garder le depalcement X en memoire
    	depXMem = depX;
    	//garder le deplacement Y en memoire
    	depYMem = depY;  	
    	
    
    	
    }
    /**
	*M�thode qui M�thode qui permet de retourner information de la forme 
	*demi-droite
	*
	*@return String  Retourne un information de la forme demi-droite
	*/
    public String toString(){
    	return ("Demi-Droite" + "\n"+"Points:(" + p1.getX() + "," + p1.getY() +
    			")" + " " + "(" + p2.getX() + "," + p2.getY() + ")" + 
    			"Couleur:" + super.getCouleur() + "\n");
    }
    
    
    

}
