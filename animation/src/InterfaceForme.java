import java.awt.Graphics2D;
import java.io.Serializable;

/**
 * Une forme � ajouter dans une sc�ne impl�mente 
 * obligatoirement cette interface.
 * 
 * Serializable : Sert � la sauvegarde
 */
public interface InterfaceForme extends Serializable{

	/**Retourne si la forme contient le point fourni
	*Note : Cette m�thode est appel�e pour localiser une formedans une sc�ne.*/	
	public boolean contient(Point2D p);
	
	/**La forme se dessine elle-m�me dans l'objet graphique re�u*/
    public void dessine(Graphics2D g2);
	
    /**D�place la forme et l'am�ne au point fourni*/
    public void deplace(Point2D pointDepart, Point2D pointArrivee);
    
    public String toString();
}
