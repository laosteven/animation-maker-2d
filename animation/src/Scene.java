import java.awt.Graphics2D;
import java.io.Serializable;


/**
*M�thode permettant de regrouper diff�rentes formes dans un tableau, de savoir
*combien de formes se tableau contient, qu'elle est la premi�re forme � 
*certaine position, de lui ajouter des formes ou d'en soustraire.
* 
*@author Alexandre Audette Genier
*@version 1 mars 2013
*
*/
public class Scene implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	CONSTANTE
	*/
	//scene contient aucune forme
	final static int AUCUNE_FORME = -1;
	
	//nombre de forme ar d�fault dans le tableau interfaceforme
	final private int MAX_FORMES = 100;
	
	
	
	/*
	ATTRIBUTS
	*/	
	//tableau de forme
	private InterfaceForme[] tabFormes; 
	
	//informatuion du nombre de forme dans le tableau
	private int nbFormes;
	
	/*
	CONSTRUCTEURS
	*/
	/**********************************************************************
	*Constructeur par copie d'attribut mes le tableau de formes � un maximum
	*de 500.
	*
	*********************************************************************/
	public Scene(){
		tabFormes  = new InterfaceForme[MAX_FORMES];
	}
	/**********************************************************************
	*Constructeur avec un tableau et un nombre de forme pr�d�finient.
	* 	
	*@param TableauRecu Tableau de forme
	*@param nbFormes    Nombre de forme de d�part
	*********************************************************************/
	public Scene(InterfaceForme[] TableauRecu, int nbFormes){
		tabFormes = TableauRecu;
		this.nbFormes = nbFormes;
	}
	
	
	/*
	MUTATEURS
	*/
	/**********************************************************************
	*M�thode qui permet de modifier le nombre de forme de la sc�ne
	*
	*@param nbFormes Le nombre de forme d�sir�
	*********************************************************************/	
	public void setNbFormes(int nbFormes){
		this.nbFormes = nbFormes;
	}
	/**
	*M�thode qui permet de d�truire tous les  formes du tableau	* 	
	*/
	/*NOTE � MOI-M�ME:
	 * 					1-cr�� un nouveau tableau VIDE
	 * 					2-Mettre le nombre de forme � 0.
	 *																	*/
	//detruit tous les forme du tableau
	public void detruireLesFormes(){
		//tableau effacer
		tabFormes = null;
		//Cr�ation du tablaeu vide
		tabFormes  = new InterfaceForme[MAX_FORMES];
		
		//remettre le nombre de forme du tabeau a 0
		nbFormes = 0;
	}	
	
	
	/*
 	ACCESSEURS
	*/
	/**********************************************************************
	*M�thode qui Retourne le nombre de forme de la sc�ne
	*
	*@return int Le nombre de forme de la sc�ne
	*********************************************************************/	
	public int getNbFormes(){
		return nbFormes;
	}
	
	/**
	*M�thode qui permet de retourner une forme du tableau
	* 
	*@param  laquelle       Exemple premi�re forme (laquelle = 1)
	*@return InterfaceForme Forme retourner.
	*/
	public InterfaceForme getForme(int laquelle){
		//retourne la forme s�lectionner
		return tabFormes[(laquelle -1)];
	}	
	
	
	/*
	* M�THODES
	*/
	
	/**
	*M�thode qui permet d'ajouter une forme au tableau
	* 
	*@param forme Forme ajouter au tableau.
	*/
	/*NOTE � MOI-M�ME:
	 * 					1-Aller � la premi�re case vide du tableau repr�sent�
	 * 					par nbFormes.
	 * 					2-Ajouter la forme 
	 * 					3-Augmenter le nombre de forme.
	 *																	*/
	public void AjouterForme(InterfaceForme forme){
		//Ajouter forme � la case vide
		tabFormes[nbFormes]= forme;
		//Augmenter nbFormes
		nbFormes ++;		
	}
	/**
	*M�thode qui permet de supprimer une forme du tableau
	* 
	*@param laquelle Exemple premi�re forme (laquelle = 1)
	*/
	/*NOTE � MOI-M�ME:
	 * 					1-Aller � la case  du tableau � d�truire 
	 * 					2-D�caler tableau vers la gauche pour supprimer
	 * 					3-diminuer le nombre de fomre.
	 *																	*/
	public void detruireForme (int laquelle){
		//iterateur du tableau de sc�ne
		int indice = (laquelle-1);
		
		//Tant que l'indice n'est pas  � la fin du tableau
		while (tabFormes[indice+1] != null) {
			//copie le contenue dans la case suivant
			tabFormes[indice] = tabFormes[indice + 1];
			//prochaine case � gauche
			indice++;
		}
			//Mettre la derniere case a null pour eviter double
		 	//ou situation de la derniere forme aussi
			tabFormes[indice] = null;
			
			//Diminuer le nombre de sc�ne
			nbFormes--;
			
	}				
	/**
	*M�thode qui permet de localiser la premiere forme qui contient le point 
	*recu dans notre tableau de forme et retourne la case du tabeau contenant 
	*la forme a ce point contenant ce point.
	* 
	*@param  point Indice de a case de la forme a supprimer
	*@return int   Case la forme contenant le point (ex:case 0 =forme 1) 
	*/
	/*NOTE � MOI-M�ME:
	 * 					1-�valuer de la derni�re case � la premi�re(nbFormes-1)
	 * 					2-Arr�ter d'�valuer lorsque point trouver
	 * 					3-retourner l'indice de la case contenant point 					
	 *																	*/
	public int localiserForme(Point2D point){
		//Indice la case a �valer
		int caseEvaluer = nbFormes - 1;
		
		//Si le point a �t� trouver
		boolean pointTrouver =false;
		
		//tant que le point n'est pas trouver ou tableau fini
		while(!pointTrouver && caseEvaluer >=0){
			
			//si cette cse contient une forme
			if(tabFormes[caseEvaluer] != null){
				//si le point est contenue dans la forme de la case
				if(tabFormes[caseEvaluer].contient(point)){
					//position trouver
					pointTrouver =true;
				//sinon on se delace d'une case vers la gauche	
				}else{
					caseEvaluer--;
				}	
			
			}
			//sion on se d�place d'une case vers la gauche
			else {
				caseEvaluer--;
			}
		}
		
		//retourne le num�ro de la forme qui contient le point
		return (caseEvaluer);
	}
	/**
	*M�thode qui permet de retourner une copie de la classe Sc�ne
	*
	*@return Scene  Retourne une copie de sc�ne
	*/
	public Scene cloneScene(){
		//copie de la classe scene		
		//retoure la copie de Scene
		return  new Scene(tabFormes, nbFormes);		
	}
	/**
	*M�thode qui permet de retourner information de la Sc�ne dans un string
	*
	*@return String  Retourne un information de la sc�ne
	*/
	public String toString(){
		
		return ("Scene: \n"+"Nombre de forme:" + nbFormes +"\n");
	}
}
