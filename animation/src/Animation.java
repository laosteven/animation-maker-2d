
import java.io.Serializable;
/**
 *Classe qui permet de cr�� un objet animation qui contient plusieurs sc�ne
 *dans un  tableau dynamique.
 *   
 *@author Alexandre Audette G�nier
 *@version H2013
 *
 */

public class Animation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 CONSTANTE
	*/
	
	//Le nombre maximum de sc�nes dans le tableau de sc�nes
	public static final int MAX_SCENE = 500;
	
	//Lisibilit� du code
	public static final int AUCUNE_SCENE_ACTIVE = -1;
	
	//Vitesse au d�part
	public static final int VITESSE_DEPART = 300;
	
	/*
	 ATTRIBUTS
	*/	
	
	//Tableau des sc�nes active
	private Scene[] tableauScene;
	
	//Nombre de sc�ne(s) contenue 
	private int nbScene;
	
	//La  sc�ne active
	private int sceneActive;
	
	//Vitesse d'activation d'une sc�ne
	private int vitesseActivation;
	

	
	
	/*
	CONSTRUCTEURS
	*/	
	
	/************************************************************************
	*Constructeur par d�fault qui cr� une animation avec un sc�ne vide, active  
	*cette sc�ne et met la vitesse par d�fault.	 	
	 ************************************************************************/	
	/*NOTE � MOI-M�ME:
	 * 					1-Cr�� le tableau de sc�ne (max 500).
	 * 					2-Mettre l'indice de la sc�ne active a 0.
	 * 					3-Cr�� une sc�ne vide de formes et palcer dans le 
	 * 						tableau de sc�ne(sc�ne active).
	 * 					4-Nombre de sc�ne mis � 1. 					
	 * 					5-Vitesse mise � sa valeur par d�fault. */
	public Animation() {
		//remise de tous les param�tre � default
		parametreDefault();			
	}
	
	/*
	ACCESSEURS
	*/
	/************************************************************************
	*M�thode qui Retourne la vitesse d'activation d'une sc�ne.
	* 
	*@return int La vitesse d'activation des sc�nes 		
	************************************************************************/
	public int getVitesse() {
		//retourne la vittesse d'activation
		return vitesseActivation;
				
	}
	/************************************************************************
	*M�thode qui Retourne le nombre sc�ne contenue dans animation.
	*
	*@return int Le nombre de sc�ne dans l'animation		
	************************************************************************/
	public int getNbScene() {
		//retourne le nombre de sc�ne contenue dans animation
		return nbScene;
		
	}	
	
	/************************************************************************
	*M�thode qui Retourne le numero de la sc�ne active.
	*
	*@return int Le numero de la scene active	
	************************************************************************/
	public int getNumSceneActive() {
		//retourne le numero de la scene active
		return sceneActive;
		
	}	
	/************************************************************************
	*M�thode qui retourne la sc�ne qui est pr�sentement active dans  
	*l'animation.
	* 			
	*@return Scene La scene pr�sentement active.		
	************************************************************************/	
	/*NOTE � MOI-M�ME:	 	
	 * 1-retourner la scene contenue dans le tableau � l'indice Active
	 * 																	*/
	public Scene getSceneActive() {		
		//retourner la sc�ne Active
		return tableauScene[sceneActive];		
		
	}
	
	/*
	UTILITAIRES
	*/
	/************************************************************************
	*M�thode qui cr� une nouvelle sc�ne et la rend active.
	* 					
	************************************************************************/	
	/*NOTE � MOI-M�ME:	 	
	 *					1-L'indice o� copier la sera = sceneActive.
	 *					2-Lib�rer la case � droite de la sc�ne Active 
	 *					  si pas libre.----> if == null else d�caler(Sous-Prog)
	 *					3-Cr�� nouvelle sc�ne dans la case de droite (libre) 
	 *					4-sceneActive = cette indice (++)
	 *					5-Augmenter nbScene
	 * 																	*/
	public void nouvelleScene(){
				
		 
		//v�rifier si case suivant de sc�ne Active est libre
		if(tableauScene[(sceneActive + 1)] == null){
				
			//Nouvelle sc�ne  dans case de droite
			tableauScene[(sceneActive + 1)] = new Scene();					 
						
		}else {
				
			//Decaler tableau vers la droite apres sc�ne Active
			decalerVersDroite(sceneActive);
				
			//Copier dans case de droite
			tableauScene[(sceneActive + 1)] =  new Scene();
						
		}	
			
		//Changer la sc�ne Active
		sceneActive ++;
		//Augmenter nbScene
		nbScene ++;		
		
	}
	
	/************************************************************************
	*M�thodes qui permet de copier une sc�ne active et de mettre cette 
	*copie � la droite de celle-ci. De plus, la copie devient la sc�ne active.
	* 				
	************************************************************************/	
	/*NOTE � MOI-M�ME:	 	
	 * 					1-Utiliser cloneScene() pour la copie ??
	 *					2-L'indice o� copier la sera = sceneActive.
	 *					3-Lib�rer la case � droite de la sc�ne Active 
	 *					  si pas libre.----> if == null else d�caler(Sous-Prog)
	 *					4- Copi� dans la case de droite (libre) 
	 *					5- sceneActive = cette indice
	 *					6-Augmenter nbScene
	 * 																	*/
	public void copierSceneActive(){
		//v�rifier si case suivant de sc�ne Active est libre
		if(tableauScene[(sceneActive + 1)] == null){
			
			//Copier dans case de droite
			tableauScene[(sceneActive + 1)] =
					tableauScene[sceneActive].cloneScene();
		}else {
			
			//Decaler tableau vers la droite apres sc�ne Active
			decalerVersDroite(sceneActive);
			
			//Copier dans case de droite
			tableauScene[(sceneActive + 1)] =
					tableauScene[sceneActive].cloneScene();
		}
		//Augmenter nbScene
		nbScene ++;
		
		//Changer la sc�ne Active
		sceneActive ++;
	}
	
	/************************************************************************
	*M�thode qui permet de d�caler les donn�es contenue dans le tableau
	*� partir d'un indice re�u de une case vers la droite.
	* 			
	*@param indice Endroit o� commencer le d�placement vers la droite.		
	************************************************************************/
	/*NOTE � MOI-M�ME:	 	
	 * 					1-L'indice de la sc�ne re�u
	 *					2-Copier Le nombre de forme dans le tableau 
	 *						pour l'indice o� commencer.
	 *					3-Copier le contenue de la case dans droite de l'indice
	 *						o� commencer et d�cr�menter cette indice
	 *					4-R�p�ter tant que l'indiced�part  > indice
	 *					
	 * 																	*/
	private void decalerVersDroite(int indice){
		//indice ou commencer � d�caler
		int indiceDepart = nbScene - 1;
		
			//Tant que l'indice n'est pas rendu � la sceneActive
			while (indiceDepart > indice ) {
				
				//copie le contenue dans la case suivant
				tableauScene[(indiceDepart + 1)] = tableauScene[indiceDepart];
				
				//procahine case � gauche
				indiceDepart--;
			}
		
	}	
	
	
	/*
	MUTATEURS
	*/
	/************************************************************************
	*M�thode qui permet d'ajuster la vitesse de d�filement
	*@param vitesseActivation Nouvelle vitesse d'activation.	
	************************************************************************/	
	public void setVitesse(int vitesseActivation) {
		//Ajuste la vitesse
		this.vitesseActivation = vitesseActivation;
		
	}
	/************************************************************************
	*M�thode qui active la sc�ne suivante
	*strategie:
	*		Circulaire si la scene active est la deniere scene alors
	*		retour a la premiere scene.
	* 		
	************************************************************************/	
	public void activerSceneSuivante() {
	//strategie:
	//			Circulaire si la scene active est la deniere scene alors
	//			retour a la premiere scene.
		
	//Sc�ne suivante active
		if(sceneActive < (nbScene -1) ){
			//scene suivante
			sceneActive ++;
		}else {
			//retour a la premiere scene
			sceneActive = 0;
		}
		
	}
	/************************************************************************
	*M�thode qui active sc�ne pr�c�dente
	*strategie:
	*			Si la scene active est la premiere scene alors ne pas 
	*		    deplacer la scene active vers la precedente. 		
	************************************************************************/	
	public void activerScenePrecedente() {
	//strategie:
	//			Si la scene active est la premiere scene alors ne pas 
	//		    deplacer la scene active vers la precedente.
		
		if(sceneActive != 0){
		//Sc�ne pr�c�dente active
		sceneActive --;
		}
		
	}
	/************************************************************************
	*M�thode qui active la premi�re sc�ne
	* 		
	************************************************************************/	
	public void activerPremiereScene() {
		//Premi�re sc�ne active
		sceneActive = 0;
		
	}
	/************************************************************************
	*M�thode qui active la derni�re sc�ne
	* 		
	************************************************************************/
	public void activerDerniereScene() {
		//Derni�re sc�ne active
		sceneActive = nbScene -1;
			
	}
	
	/************************************************************************
	*M�thode qui permet de d�truire la sc�ne active sauf si celle-ci
	*est la derni�re sc�ne contenue dans l'animation.
	* 				
	************************************************************************/	
	/*NOTE � MOI-M�ME:	 	
	* 					1-Pas d�truire si sceneActive = 0 			
	*					2-D�placer les sc�ne contenue vers la gauche dans le 
	*					  tableau � partir de la sc�ne active pour la supprimer.
	*					3-Diminuer le nombre de scene.
	*					4-Sc�ne active est mise  � la derni�re du tableau
	*					
	* 																	*/
	public void detruireSceneActive() {
		
		//iterateur du tableau de sc�ne
		int indice = sceneActive;
		
		//Tant que l'indice n'est pas  � la fin du tableau
		while (tableauScene[indice+1] != null) {
			//copie le contenue dans la case suivant
			tableauScene[indice] = tableauScene[indice + 1];
			//prochaine case � gauche
			indice++;
		}
		
		//mettre la derniere case a null pour eviter double
		tableauScene[indice] = null;
		//modifier nombre de sc�ne
		nbScene--;
		//modifier sc�ne active
		sceneActive = nbScene - 1;
		
	}
	/************************************************************************
	*M�thode qui permet de d�truire tous les sc�nes d'une animation et 
	*d'en garder une seul vide et active.
	* 				
	************************************************************************/	
	/*NOTE � MOI-M�ME:	 	
	* 					1- cr�� un nouveau tableau par principe de constructeur
	*
	* 																	*/
	public void detruireLesScenes() {
		//remise de tous les param�tre � default
		parametreDefault();					
	}	
	
	/**
	*M�thode qui permet de retourner information de l'animation dans un string
	*
	*@return String  Retourne un information de l'animation
	*/
	public String toString(){
		return("Animation: \n"+"Nombre de Scene: " + nbScene +"\n" +
			   " La scene Active: " + sceneActive + "\n" +
			   " La vitesse de defilement: " + vitesseActivation +"\n");
	}
	
	/**
	*M�thode priv� qui permet de remttre tous les param�tres d'une Animation aux 
	*param�tre par default.
	*
	*/
	private void parametreDefault(){
		//Cr�ation du tableau de sc�ne
		tableauScene = new Scene[MAX_SCENE];
		
		//Indice  de la sc�ne active
		sceneActive = 0;
		
		//Cr�ation de la sc�ne vide et mettre dans tableau
		tableauScene[sceneActive] = new Scene();
		
		//Nombre de sc�ne contenue mis a 0.
		nbScene = 1;			
			
		//Vitesse mise � la valeur par d�fault.
		vitesseActivation = VITESSE_DEPART;		
		
	}
	
	

}
