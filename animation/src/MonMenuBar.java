

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
/**
*Classe qui permet de cr�� un objet JMenuBar qui cree une bar de menu et qui
*contient les differents items permettsnt l'ouverture, la sauvegarde et la 
*creation d'une nouvelle animation.
*   
*@author Alexandre Audette G�nier
*@version H2013
*
*/
public class MonMenuBar extends JMenuBar implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	CONSTANTE
	*/
	

	/*
	ATTRIBUTS
	*/	
	//referencec vers le menu principale
	private PanneauPrincipal refMenuPrincipal;	
	
	//Onglet acceder au option fichier
	private JMenu fichierOption = new JMenu("Fichier");
	
	//Onglet acceder au option animation
	private JMenu animationOption = new JMenu("Animation");
	
	//Item dans FichierOption pour sauvegarde
	private JMenuItem ecouteurMenuSauvegarde = new JMenuItem("Sauvegarder");
	
	//Item dans FichierOption pour Ouvrir une animation
	private JMenuItem ecouteurMenuOuvrir = new JMenuItem("Ouvrir");
	
	//Item dans AnimationOption pour cree une animation vide
	private JMenuItem ecouteurMenuNouvAnimation = new JMenuItem("Nouvelle"+
																" animation");
	//objet permettant utiliser un selectionneur de fichier
	private static JFileChooser selectionFichier = new JFileChooser();
	
	//nom fichier texte a sauvegardeer
	private String nomFichierTxt = null;
	
	//fichier bien enrigistrer
	private int select;
	
	//filtre fichier txt
	private FileNameExtensionFilter filtreTxt;
	/*
	CONSTRUCTEURS
	*/	
		
	/************************************************************************
	*Constructeur par d�fault qui cr� un menu bar et guarde la reference
	*du panneau principal.
	*@param panneauPrincipaleAff Reference du panneau principal.	
	************************************************************************/		
	public  MonMenuBar(PanneauPrincipal panneauPrincipaleAff){
		
		//reference ajustere au panneau princiale
		refMenuPrincipal = panneauPrincipaleAff;	
		
		//abonner l'item ouvrir a l'ecoteur du menu
		ecouteurMenuOuvrir.addActionListener(this);
		//abonner l'item sauvegarde a l'ecouteur du menu
		ecouteurMenuSauvegarde.addActionListener(this);
		//abonner l'item nouvelle animation a l'ecouteur du menu
		ecouteurMenuNouvAnimation.addActionListener(this);
		
		//ajouter l'option ouvrir a fichierOption
		fichierOption.add(ecouteurMenuOuvrir);
		
		//ajouter l'option sauvegarde a fichierOption
		fichierOption.add(ecouteurMenuSauvegarde);		
		
		//ajouter l'onglet fichier au menu
		add(fichierOption);		
		
		//ajouter l'option nouvelle animation a AnimationOption
		animationOption.add(ecouteurMenuNouvAnimation);
		
		//ajouter l'onglet Animation au menu
		add(animationOption);
	}


	
	
		
	/*
	ACCESSEURS
	*/
	
	
		
		
		
	/*
	MUTATEURS
	*/
	
	
		
		
	/*
	M�THODES
	*/
	
	
	
	/*
	M�THODES OBLIGATOIRE PAR IMPLEMENTATION	
	*/	
	/**********************************************************************
	*Methode par implementation qui permet de gerer les action qui arrive
	*sur un objet.
	*@param e Sur quelle Objet l'action a eu lieu
	 *********************************************************************/	
	public void actionPerformed(ActionEvent e) {
		
		//si l'option ouvrir est selectionner
		if(e.getSource() == ecouteurMenuOuvrir ){			
				//Recupere animation et ajuste la reference				
				refMenuPrincipal.setRefAnimation(
						UtilitaireFichier.recupererAnimation());	
				
				//valider les changements apporter
				refMenuPrincipal.validate();
				
				//refraichir le panneau dessin
				refMenuPrincipal.getPanneauDessin().repaint();
				//actualiser l'information scene active et nbScene
				refMenuPrincipal.getPanneauSelectionSceneActive().
														actualiseNombScene();
		}
		
		//si l'option  sauvegarde est selectionner 
		if(e.getSource() == ecouteurMenuSauvegarde){
				//sauvegarder le fichier Binaire
				UtilitaireFichier.sauverAnimationBinaire(
							refMenuPrincipal.getRefAnimation());
				
				//filtre txt
				filtreTxt = new FileNameExtensionFilter("txt");
			
				//application du filtre txt
				selectionFichier.setFileFilter(filtreTxt);
				
				//R�cup�rer le nom de fichier d�une animation sauvegard�e
				select = selectionFichier.showDialog(selectionFichier, 
													 	 "Sauvegarder txt"); 				

				//Si l�utilisateur annule ou que le nom de fichier est invalide
				if(select == JFileChooser.APPROVE_OPTION) {
					//fichier ou sauver s�lectionner
					nomFichierTxt=selectionFichier.getSelectedFile().
							getPath() + ".txt";
					
				}else {
				
					//nom sauvegarde par default
					nomFichierTxt = "animation.txt";
					//message � l'�cran
					JOptionPane.showMessageDialog(null, "Fichier .txt" +
													" sauvegard� par default");
				
				}
				//ajout de l'extension
				//nomFichierTxt = nomFichierTxt + ".txt";
				//sauvegarder le fichier Txt
				UtilitaireFichier.sauverAnimationTexte(
						refMenuPrincipal.getRefAnimation(), nomFichierTxt);
				
				
		}
		
		//si l'option nouvelle animation est selectionner
		if(e.getSource() == ecouteurMenuNouvAnimation){
			
			// Tant que l'animation du programme n'est pas vide
			if(refMenuPrincipal.getRefAnimation() != null){
				
				// On cree un tableau de choix qui sera utilise pour les boutons
				String[] choix = { "Enregistrer", " Ne pas enregistrer ", 
						" Annuler " };

				// Une fenetre d'option apparait
				int confirme = JOptionPane.showOptionDialog(

						// Ecran centree
						null,

						// Message de la fenetre
						"Desirez-vous enregistrer votre travail avant"
						+ " de creer une nouvelle?",

						// Titre de la fenetre
						"Nouvelle animation",

						// Type d'option
						JOptionPane.YES_NO_CANCEL_OPTION,

						// Type de message
						JOptionPane.QUESTION_MESSAGE,

						// Icone du titre
						null,

						// Nom personnalise des boutons
						choix,

						// Bouton par defaut (Si on tape enter
						// au premier coup)
						choix[0]

				);

				// Un 'Switch Case' confirme la reponse de l'utilisateur
				switch (confirme) {

				// Enregistrement
				case 0:

					//cree une nouvelle animation vide
					//sauvegarder le fichier
					UtilitaireFichier.sauverAnimationBinaire(
					refMenuPrincipal.getRefAnimation());
					//cree la nouvelle animation
					refMenuPrincipal.setRefAnimation(new Animation());

					// Fin du case 0
					break;

				// Non - enregistrement
				case 1:

					// Une nouvelle animation est creee au panneau principal
					refMenuPrincipal.setRefAnimation(new Animation());
					break;

				// Si l'utilisateur annule
				case 2: // " Annuler "
				case -1: // Fermeture 'x' de la fenetre

					// Rien ne se passe, la fenetre se ferme simplement
					break;

				}
				
			} else{
				
				// Une nouvelle animation est creee au panneau principal
				refMenuPrincipal.setRefAnimation(new Animation());
				
			}
			
			//actualiser l'information scene active et nbScene
			refMenuPrincipal.getPanneauSelectionSceneActive().
													actualiseNombScene();

			//valider les changements apporter
			refMenuPrincipal.validate();
			
			//refraichir le panneau dessin
			refMenuPrincipal.getPanneauDessin().repaint();
		}
		
	}	
		
	/*
	CLASSES INTERNE	
	*/
	

}
