/**
 * Contient des m�thodes utiles pour le type double
 * 
 * @author pbelisle
 * @version octobre 2009
 */
public class UtilitaireReel {
	
	//Marge d'erreur pour d�tecter l'�galit�.
   	//La diff�rence entre deux position doit �tre inf�rieure � EPSILON 	
   	private static final double EPSILON = 10;
	
  	/**
   	 * Retourne si v1 et v2 sont �gaux.  Ils seront
   	 * �gaux si la diff�rence entre v1 et v2 est inf�rieure � UtilitaireReel.EPSILON
   	 * @param v1 La 1i�re valeur
   	 * @param v2 La 2i�me valeur
   	 * @return vrai si (v1 - v2) < EPSILON
   	 */
   	public static boolean equals(double v1, double v2){
   		
   		return Math.abs(v1 - v2) < EPSILON;
   	}	

}
