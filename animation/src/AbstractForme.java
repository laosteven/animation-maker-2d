import java.awt.Color;
import java.io.Serializable;
/**
 * Permet de regrouper les attributs communs � toutes les formes.
 * 
 * 
 * Note : La classe est abstraite puisque la m�thode contient() de  
 * InterfaceForme, d�pend de la forme qui est d�fini au niveau
 * des sous classes.
 * 
 * 
 * L'utilisation de cette classe est obligatoire dans le projet.
 * 
 */
public abstract class AbstractForme implements InterfaceForme, Serializable{
	
	
	/**
	 * Num�ro de version par d�faut (enl�ve un warning)
	 */
	private static final long serialVersionUID = 1L;
	
	//Toutes les formes ont une position dans l'espace 2D et une couleur
	private Point2D pos;
	
	//Ici la classe java.awt.Color est utilis�e seulement � des fins
	//p�dagogiques (vous montrer que cela existe)
	private Color couleur;
	
	/**
	 * Constructeur qui met une position (0,0) et
	 * la couleur java.awt.WHITE � une forme
	 */
	public AbstractForme(){
		pos = new Point2D(0,0);
		couleur = java.awt.Color.WHITE;
	}
	
	/**
	 * Constructeur qui fournit une position et
	 * la couleur java.awt.WHITE � une forme
	 * 
	 * @param p La position de la forme dor�navant
	 */
	public AbstractForme(Point2D p){
		pos = p;
		couleur = java.awt.Color.WHITE;
	}
	
	/**
	 * Constructeur qui fournit une position  et
	 * une couleur � une forme
	 * 
	 * @param p La position de la forme dor�navant
	 */
	public AbstractForme(Point2D p, java.awt.Color c){
		pos = p;
		couleur = c;
	}
	
	/**
	 * 
	 * Accesseur de position.  L'interpr�tation de ce que repr�sente la position 
	 * est laiss�e aux utilisateurs des objets.  Par exemple, pour une ellipse,
	 * la position peut �tre celle du centre ou d'un des coins du rectangle 
	 * qui contient l'ellispe.  M�me 
	 * chose pour un rectangle o� la position peut �tre le centre ou 
	 * n'importe lequel des quatre coins, etc.
	 * 
	 * @return La r�f�rence sur la position de la forme
	 */
	public Point2D getPos(){
		return pos;
	}
	
	/**
	 * Accesseur de couleur
	 * 
	 * @return La couleur de la forme
	 */
	public Color getCouleur(){
		return couleur;
	}
	
	/**
	 * Mutateur de la position.  Aucune validation
	 * n'est faite.  La r�f�rence est conserv�e et non une copie.
	 * @param nouvellePos
	 */
	public void setPos(Point2D nouvellePos){
	
		pos = nouvellePos;
	}
	
	/**
	 * Mutateur de la couleur
	 * 
	 * @param nouvelleCouleur La couleur dor�navant. 
	 */
	public void setCouleur(java.awt.Color nouvelleCouleur){
		couleur = nouvelleCouleur;
	}

}
