//Gestion de fichier binaire
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream; 
import java.io.ObjectOutputStream; 

//Gestion de fichier texte
import java.io.File ;
import java.io.PrintWriter; 

//Gestion d'exception
import java.io.IOException;

//Interface � l'�cran
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
/**
 *Utilitaire qui permet de sauvegarder et recuperer une animation en forme 
 *bianire. Ainsi que de sauvegarder toutes l'information d'une sc�ne en .txt
 *   
 *@author Alexandre Audette G�nier
 *@version H2013
 *
 */

public class UtilitaireFichier {
	

	/*
	 ATTRIBUTS
	*/	
	//objet permettant utiliser un selectionneur de fichier
	private static JFileChooser selectionFichier = new JFileChooser();
	
	//Object permettant decrire des objets dans un fichier
	private static ObjectOutputStream ecrireObjet; 
	
	//Fichier � ouvir pour aller �crire
	private static FileOutputStream ecrireDansFichier;
	
	//Object permmettant daller lire un objet dans un fichier
	private static ObjectInputStream lireObject;
	
	//Fichier � ouvire pour aller lire
	private static FileInputStream  lireDansFichier;
	
	//Object gestion du fichier texte
	private static File gestionFichierTexte;
	
	//Object permet d'�crire dans le fichier texte
	private static PrintWriter ecrirefichierTexte;
	
	/*
	 METHODES
	*/	
	
	/************************************************************************
	*M�thode permettant de sauvegarder un objet de type Animation.
	*dans un fichier.
	* 
	*@param animationSauver L'objet Animation � sauvegarder.
	*
	 ************************************************************************/
	/*NOTE � MOI-M�ME:
	 * 					1-Choisir la destination pour sauvegarder mon Animation
	 *  				2-Destination = nomFichier en param�tre
	 *  				3-Animation � sauvegarder = animationSauver param�tre
	 *  				4-Sauver l'animation writeObject();
	 *  				5- femrer le fichier close();
	 *  																	*/
	 public static void sauverAnimationBinaire(Animation animationSauver){
		//Selection ou Cancellation
		int SelectOuCancel;
		//Utilisateur Annule
		boolean UtilisateurAnnule = false;
		//Fichier selectionner valide
		boolean fichierValide = false;
		//fichier � ouvrir
		String nomFichier;
		
		
		//Tant que le fichier n'est pas valide
		while(!fichierValide) {
			
		
			//R�cup�rer le nom de fichier d�une animation sauvegard�e
			SelectOuCancel = selectionFichier.showDialog(selectionFichier, 
													 	 "Sauvegarder"); 

			//Si l�utilisateur annule ou que le nom de fichier est invalide
			if(SelectOuCancel == JFileChooser.APPROVE_OPTION) {
				//fichier ou sauver s�lectionner
				nomFichier = selectionFichier.getSelectedFile().getPath();
			}else {
			
				//Utilisateur a Annul�
				UtilisateurAnnule = true;
				//fichier valide
				fichierValide =true;
				//pas de fichier
				nomFichier = null;
				//Message pas de sauvegarde
				System.out.println("Fichier non sauvegard�");
				//message � l'�cran
				JOptionPane.showMessageDialog(null, "Fichier pas sauvegard�");
			
			}
			//si l'utilisateur n'annule pas
			if(!UtilisateurAnnule){
		
				try{
					//fichier o� aller �crire
					ecrireDansFichier = new FileOutputStream(nomFichier);		
					
					//fichier valide
					fichierValide =true;
					
					//s�lectionner le fichier ou �crire		
					ecrireObjet = new ObjectOutputStream(ecrireDansFichier);		
			
					//�crire l'animation dans le fichier		
					ecrireObjet.writeObject(animationSauver);
			
					//fermer le fichier
					ecrireObjet.close();	
				}
				catch(IOException e){	
					//Message pas de sauvegarde
					System.out.println("Nom du fichier de sauvegarde" +
									   " binaire non valide");
					//message � l'�cran
					JOptionPane.showMessageDialog(null,
												"Nom du fichier de sauvegarde" +
														" binaire non valide");
					//e.printStackTrace();
				}
			}
		}
		
	}
	
	/************************************************************************
	*M�thode permettant de recuperer un objet de type Animation 
	*dans un fichier.
	* 			
	*@return Animation L'animation r�cup�r�e dans le fichier.
	* 
	************************************************************************/
	/*NOTE � MOI-M�ME:
	* 					0-G�rer les Exception(2).
	* 					1-Acqu�rir emplacement du fichier par param�tre
	* 					2-Lire l'objet dans ce fichier
	*  					3-Convertir l'objet lu en type Animation
	*  					4-Retourner l'Aniamtion	*  					
	*  																	*/
	public static Animation recupererAnimation(){
		
		//Animation retourner par la lecture
		Animation AnimationARetourner;
		
		//Selection ou Cancellation
		int SelectOuCancel;
		
		//fichier � ouvrir
		String fichierOuvrir;
		
		//R�cup�rer le nom de fichier d�une animation sauvegard�e
		SelectOuCancel = selectionFichier.showDialog(selectionFichier,
													 "Recuperrer"); 

		//Si l�utilisateur annule 
		if(SelectOuCancel != JFileChooser.APPROVE_OPTION) {
			//Utilisateur cancel la s�lection de fichier
			fichierOuvrir ="";	
			
		//sinon
		}else {				 
		
			//Prendre fichier
			fichierOuvrir  = selectionFichier.getSelectedFile().getPath();		
		}     		
		
		try{					
		//Fichier o� aller lire
		lireDansFichier = new FileInputStream(fichierOuvrir);
		
		//Acc�der au fichier
		lireObject = new ObjectInputStream(lireDansFichier);
		
		//Copie de l'animation dans le fichier
		AnimationARetourner = (Animation) lireObject.readObject();
		
		}catch(IOException e){
			//Information de l'erreur de s�lection
			System.out.println("Fichier non valide fourni. Cr�ation d'une" +
							   " animation vide.");	
			//message � l'�cran
			JOptionPane.showMessageDialog(null, "Fichier non valide fourni." +
											" Cr�ation d'une animation vide.");
			//Animation retourner si erreur
			AnimationARetourner = new Animation();
		}catch(ClassNotFoundException e){			
		
			//Information de l'erreur 
			System.out.println("La classe de l'objet du fichier ouvert" +
							   " n'est pas une Animation");
			//message � l'�cran
			JOptionPane.showMessageDialog(null, "Fichier non valide fourni." +
											" Cr�ation d'une animation vide.");
			//Animation retourner si erreur
			AnimationARetourner = new Animation();
		}
		
		//retourner l'animation recuperer dans le fichier
		return AnimationARetourner;
	}
	/************************************************************************
	*M�thode permette de sauver tous les formes contenu dans les  
	*diff�rentes sc�ne d'une animation.
	* 
	*@param animationSauver L'animation contenant tous les formes � sauver.
	*@param nomFichier      Path du Fichier o� sauvegarder l'information.	
	 ************************************************************************/
	/*NOTE � MOI-M�ME:
	 * 					1-Choisir la destination pour sauvegarder l'information
	 *  				2-Rendre le fichier possible � l'�criture
	 *  				3-ecrire info de l'animation
	 *  				4-tant que pour tous les scene ecrire info
	 *  				5-tant que pour tout les formes ecrire info ( dans -4)
	 *  				6- femrer le fichier close();
	 *  																	*/	
	public static void sauverAnimationTexte(Animation animationSauver, 
											String nomFichier) {
		//iterateur pour le nombre de scene
		int nbSceneInscrite = 0;
		
		//iterateur pour le nombre de forme
		int nbFormeInscrite = 1;
		
		//acc�der au fichier � �crire
		gestionFichierTexte = new File(nomFichier);
		
		//nom de fichier valide
		boolean nomFichierValide = false;
		
		//fichier txt en cas d'erreur
		String fichierTxt = null;
		

			
		while(!nomFichierValide) {
			try{
			//fichier dans lequel ecrire
			ecrirefichierTexte = new PrintWriter(gestionFichierTexte);
			//fichier est valide
			nomFichierValide = true;
			}
			catch(FileNotFoundException e){	
				//message � l'�cran
				JOptionPane.showMessageDialog(null,"Nom fichier de" +
						" sauvegarde texte non valide");
				//message console
				System.out.println("Nom fichier de sauvegarde " +
								   "texte non valide");
			}
			
			//si le fichier est invalide
			if(!nomFichierValide){				 

				//Si l�utilisateur annule 
				if(selectionFichier.showDialog(selectionFichier,
							 "SauvegardeTxT") != JFileChooser.APPROVE_OPTION) {
				
					//Utilisateur cancel la s�lection de fichier
					fichierTxt ="";	
					//acc�der au fichier � �crire
					gestionFichierTexte = new File(fichierTxt);
						
				//sinon
				}else {				 
					
					//Prendre fichier
					fichierTxt  = selectionFichier.getSelectedFile().
																	getPath();
					//nouveau fichier txt
					gestionFichierTexte = new File(fichierTxt);
				}     		
					
			}
			
		}
		
		//Ecrire information de la forme dans le fichier
		ecrirefichierTexte.println(animationSauver);
		
		//se placer a la premier scene
		animationSauver.activerPremiereScene();
			
		//boucle pour le nombre de scene contenue dans animation
		while(nbSceneInscrite < animationSauver.getNbScene()){
			
			//inscrire l'information de la scene
			ecrirefichierTexte.println(animationSauver.getSceneActive());				
				
			//boucle pour le nombre de forme dans scene
			for( nbFormeInscrite = 1; nbFormeInscrite <= 
				 animationSauver.getSceneActive().getNbFormes() ;
				 nbFormeInscrite ++) {
					
				//inscire l'information de la forme
				ecrirefichierTexte.println(animationSauver.getSceneActive().
						 							getForme(nbFormeInscrite));					
					
				
			}
				
			//prochaine scene
			nbSceneInscrite ++;
			//se placer a la scene suivante
			animationSauver.activerSceneSuivante();
				
			
		}	
		
		//fermer fichier
		ecrirefichierTexte.close();		
		//Emplacement du fichier texte
		System.out.println(gestionFichierTexte.getAbsolutePath());
		
		//affichage de l'emplacement � l`�cran		
		JOptionPane.showMessageDialog(null, "Fichier texte sauvegard�: \n" +
										gestionFichierTexte.getAbsolutePath());
		
		
	}
	

}
