import java.awt.Graphics2D;
/**
*Classe qui h�rite de la classe AbstractForme et qui impl�mente Interfarce 
*forme. Permet de cr�� une figure de la forme d'un rectangle et de savoir les
*points qu'il contient.
* 
*
* 
*@author Alexandre Audette Genier
*@version 17 f�vrier 2013
*
*/

public class Rectangle extends AbstractForme {

	/*
	CONSTANTE
	*/
	/*
	ATTRIBUTS
	*/
	
	//valeur de la hauteru du rectangle
	private double hauteur;
	//valeur de la largeur du rectangle
	private double largeur;
	
	/*
	CONSTRUCTEURS
	*/	
	//constructeur par default
	public Rectangle() {
		
	}
	/**********************************************************************
	*Constructeur par copie d'attribut et re�oit les attributs de son parent
	* 
	*@param p Objet de type Point2D d�sir�(Point du milieu)
	*@param c Objet de type java.awt.Color d�sir�
	*@param hauteur Hauteur d�sir�e du rectangle
	*@param largeur Largeur d�sir�e du rectangle
	*********************************************************************/
	//constructeur par copie du parent
	public Rectangle(Point2D p, double hauteur, double largeur,
				     java.awt.Color c ) {
		super(p,c);
		this.hauteur = hauteur;
		this.largeur = largeur;
	}
	/*
	MUTATEURS
	*/
	/**********************************************************************
	*M�thode qui Modifier la hauteur du rectangle
	*
	*@param hauteur La hauteur d�sir�e du rectangle
	*********************************************************************/
	public void setHauteur(double hauteur) {
		this.hauteur = hauteur;
	}
	/**********************************************************************
	*M�thode qui Modifier la largeur du rectangle
	*
	*@param largeur La largeur d�sir�e du rectangle
	*********************************************************************/
	public void setLargeur(double largeur) {
		this.largeur = largeur;
	}
	/*
	ACCESSEURS
	*/
	/**********************************************************************
	*M�thode qui Retourne la hauteur du rectangle
	*
	*@return hauteur La hauteur du rectangle
	*********************************************************************/	
	public double getHauteur() {
		return hauteur;
	}
	
	/**********************************************************************
	*M�thode qui Retourne la largeur du rectangle
	*
	*@return largeur La Largeur du rectangle
	*********************************************************************/	
	public double getLargeur() {
		return largeur;
	}
	/**********************************************************************
	*M�thode qui Retourne le coin sup�rieur gauche du rectangle
	*
	*@return Point2D Retourne l'objet de type Point2D repr�sentant le coin
	*********************************************************************/	
	public Point2D getCoinSuperieurGauche() {		
		
		//calcul du point x superieur gauche
		double pointXCoinGauche = super.getPos().getX() - (largeur / 2);
		//calcul du point y superieur gauche
		double pointYCoinGauche = super.getPos().getY() + (hauteur /2);
		
		return new Point2D( pointXCoinGauche,pointYCoinGauche);
	}
	
	/*
	M�THODES
	*/	
	/**********************************************************************
	*M�thode qui M�thode qui permet de v�rifier  si un point est contenue 
	*dans le rectangle
	* 
	*@param  p       Objet de type POINT2D test�
	*@return boolean Si le point est contenue ou non dans rectangle
	*********************************************************************/
	public boolean contient(Point2D p){
		
		//point central du rectangle en x
		double positionXCentre = super.getPos().getX();
		
		//point centrale du rectangle en y
		double positionYCentre = super.getPos().getY();
		
		//si le point est contenue dans le rectangle
		if( !(p.getX() < ( positionXCentre - (largeur/2))) &&
			!(p.getX() > ( positionXCentre + (largeur/2))) &&
			!(p.getY() < ( positionYCentre - (hauteur/2))) &&
			!(p.getY() > ( positionYCentre + (hauteur/2)))) {
			return true;
		}
		//si le point nest pas dans le rectangle
		else
		{
		 return false;	
		}
		
		
	}
		
	/**
	*M�thode qui permet de dessiner un rectangle.
	*
	*@param Graphics2D Objet utiliser pour dessiner le rectangle
	*/
	public void dessine(Graphics2D g2){
		
		// Couleur du rectangle
		g2.setColor(getCouleur());
		
		//dessiner rectangle
		g2.fillRect((int)super.getPos().getX(),(int)super.getPos().getY(),
					(int)largeur,(int) hauteur);
		
	}
		
	/**
	*M�thode qui permet de dessiner un rectangle.
	*
	*@param Point2D point de depart du deplacement
	*@param Point2D point d'arriver du deplacement
	*/
	public void deplace(Point2D pointDepart, Point2D pointArrivee){
		//changement de la position au point d'arriver
		super.setPos(pointArrivee);
		
	}
	/**
	*M�thode qui permet de retourner information de la forme Rectangle
	*
	*@return String  Retourne un information de la forme Rectangle
	*/
    public String toString(){
    	return ("Rectangle" + "\n" +"Position:"+"(" + super.getPos().getX() +
    			"," + super.getPos().getX() + ")" + "Hauteur:"+ hauteur +
    			"Largeur:"+ largeur + "Couleur:" + 
    			super.getCouleur() + "\n");
    }
}
