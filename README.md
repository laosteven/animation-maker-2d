# 2D Animation Maker #
-------
Build a simple program about 2D animation making which allows the following tools:

* **Draw or move** various geometrical shapes;
* Set the animation **frame speed**;
* Edit the **color** of a shape;

Each frame can be copied or erased. At the end, once users are satisfied with their work, they may **save their progress** in a file and reopen it anytime they want with the same application.

![111.png](https://bitbucket.org/repo/4k5R5z/images/1118509581-111.png)

# Objectives #
-------
* Follow the basis of **Object Oriented** rules.
* Understanding coordinates and vectors.
* Experiment `Java Swing`.

# How do I get set up? #
-------
A runnable `.jar` file is available in the project's root folder.

# For more information #
-------
Visit the following website: [**Object Oriented Programming** (INF111)](https://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=INF111) [*fr*]